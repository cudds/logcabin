/* Copyright (c) 2012 Stanford University
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR(S) DISCLAIM ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL AUTHORS BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <gtest/gtest.h>
#include <thread>

#include "build/Protocol/Client.pb.h"
#include "Core/ProtoBuf.h"
#include "Protocol/Common.h"
#include "RPC/Buffer.h"
#include "RPC/ZeromqClientRPC.h"
#include "RPC/ZeromqClientSession.h"
#include "Server/Globals.h"

namespace LogCabin {
namespace Server {
namespace {

using Protocol::Client::OpCode;
typedef RPC::ZeromqClientRPC::Status Status;

class ServerClientServiceTest: public ::testing::Test {
	ServerClientServiceTest()
			: globals(), session(), thread() {
	}

	// Test setup is deferred for handleRPCBadOpcode, which needs to bind the
	// server port in a new thread.
	void init() {
		terminate_router = std::make_shared<bool>(false);
		zeromqRouter = std::thread(LogCabin::RPC::start_router, "61023",
				"5555", terminate_router);

		if (!globals) {
			globals.reset(new Globals());
			globals->config.set("storageModule", "Memory");
			globals->config.set("storagePath", "/mnt/ramdisk");
			globals->config.set("uuid", "my-fake-uuid-123");
			globals->config.set("servers", "127.0.0.1");
			globals->config.set("smID", "sm1");
			globals->config.set("routerBackPort", "5555");
			globals->config.set("serverID", 1);
			globals->config.set("selfAddress", "128.138.244.47:61023");

			globals->init();
			session = RPC::ZeromqClientSession::makeSession(
					RPC::Address("127.0.0.1", 61023), "sm1");
			thread = std::thread(&Globals::run, globals.get());
			sleep(1);
		}
	}

	~ServerClientServiceTest() {
		if (globals) {
			globals->eventLoop.breakRun = true;
			globals->eventLoop.exit();
			thread.join();
		}
		//added by ning
		if (globals) {
			globals->exitProxy();
			//globals->exitRouter();
		}
		if (terminate_router)
			*terminate_router = true;
		if (zeromqRouter.joinable()) {
			zeromqRouter.join();
			VERBOSE("router joined");
		}

	}

	void call(OpCode opCode, const google::protobuf::Message& request,
			google::protobuf::Message& response) {
		while(true) {
			RPC::ZeromqClientRPC rpc(session,
				Protocol::Common::ServiceId::CLIENT_SERVICE, 1, opCode,
				request);

			Status status = rpc.nonBlockWaitForReply(&response, NULL);
			if(status == Status::OK){
				break;
			}
		}

	}

	std::unique_ptr<Globals> globals;
	std::shared_ptr<RPC::ZeromqClientSession> session;
	std::thread thread;
	std::shared_ptr<bool> terminate_router;
	std::thread zeromqRouter;
};

TEST_F(ServerClientServiceTest, handleRPCBadOpcode) {
	Protocol::Client::GetSupportedRPCVersions::Request request;
	Protocol::Client::GetSupportedRPCVersions::Response response;
	int bad = 255;
	OpCode unassigned = static_cast<OpCode>(bad);
	EXPECT_DEATH( {
		init()
		;
		call(unassigned, request, response)
		;
	},
	"request.*invalid");
}

////////// Tests for individual RPCs //////////

TEST_F(ServerClientServiceTest, getSupportedRPCVersions) {
	//LogCabin::Core::Debug::setLogPolicy({{"Server", "VERBOSE"},{"RPC", "VERBOSE"}});
	init();
	Protocol::Client::GetSupportedRPCVersions::Request request;
	Protocol::Client::GetSupportedRPCVersions::Response response;
	call(OpCode::GET_SUPPORTED_RPC_VERSIONS, request, response);
	EXPECT_EQ("min_version: 1"
			"max_version: 1", response);
}

}  // namespace LogCabin::Server::<anonymous>
}  // namespace LogCabin::Server
}  // namespace LogCabin
