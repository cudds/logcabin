#include <string.h>

#include "build/Protocol/Client.pb.h"
#include "RPC/Buffer.h"
#include "RPC/ProtoBuf.h"
#include "RPC/ZeromqServerRPC.h"
#include "Server/RaftConsensus.h"
#include "Server/ClientService.h"
#include "Server/Globals.h"
#include "Server/StateMachine.h"

namespace {

/**
 * Return the time since the Unix epoch in nanoseconds.
 */
uint64_t timeNanos() {
	struct timespec now;
	int r = clock_gettime(CLOCK_REALTIME, &now);
	assert(r == 0);
	return uint64_t(now.tv_sec) * 1000 * 1000 * 1000 + now.tv_nsec;
}

}

namespace LogCabin {
namespace Server {

ClientService::ClientService(Globals& globals, std::string smID)
		: globals(globals), smID(smID){
}

ClientService::~ClientService() {
}

void ClientService::handleRPC(RPC::ZeromqServerRPC rpc) {

}

void ClientService::handleRPC(RPC::ZeromqServerRPC rpc, RPC::ZeromqPub& pub) {
	using Protocol::Client::OpCode;

	// TODO(ongaro): If this is not the current cluster leader, need to
	// redirect the client.

	// Call the appropriate RPC handler based on the request's opCode.
	switch (rpc.getOpCode()) {
		case OpCode::GET_SUPPORTED_RPC_VERSIONS:
			getSupportedRPCVersions(std::move(rpc), pub);
			break;
		case OpCode::GET_CONFIGURATION:
			getConfiguration(std::move(rpc), pub);
			break;
		case OpCode::SET_CONFIGURATION:
			setConfiguration(std::move(rpc), pub);
			break;
		case OpCode::OPEN_SESSION:
			openSession(std::move(rpc), pub);
			break;
		case OpCode::READ_ONLY_TREE:
			readOnlyTreeRPC(std::move(rpc), pub);
			break;
		case OpCode::READ_WRITE_TREE:
			readWriteTreeRPC(std::move(rpc), pub);
			break;
		case OpCode::READ_ONLY_DATA:
			readOnlyDataRPC(std::move(rpc), pub);
			break;
		case OpCode::READ_WRITE_DATA:
			readWriteDataRPC(std::move(rpc), pub);
			break;
		case OpCode::READ_WRITE_KD:
			readWriteKDRPC(std::move(rpc), pub);
			break;
		case OpCode::READ_ONLY_KD:
			readOnlyKDRPC(std::move(rpc), pub);
			break;
		default:
			rpc.rejectInvalidRequest(pub);
	}
}

std::string ClientService::getName() const {
	return smID + ":ClientService";
}

/**
 * Place this at the top of each RPC handler. Afterwards, 'request' will refer
 * to the protocol buffer for the request with all required fields set.
 * 'response' will be an empty protocol buffer for you to fill in the response.
 */
#define PRELUDE(rpcClass) \
    Protocol::Client::rpcClass::Request request; \
    Protocol::Client::rpcClass::Response response; \
    if (!rpc.getRequest(request)) \
        return;

////////// RPC handlers //////////

void ClientService::getSupportedRPCVersions(RPC::ZeromqServerRPC rpc, RPC::ZeromqPub& pub) {
	PRELUDE(GetSupportedRPCVersions);
	response.set_min_version(1);
	response.set_max_version(1);
	rpc.reply(response, pub);
}

typedef RaftConsensus::ClientResult Result;
typedef Protocol::Client::Command Command;
typedef Protocol::Client::CommandResponse CommandResponse;

std::pair<Result, uint64_t> ClientService::submit(RPC::ZeromqServerRPC& rpc,
		const google::protobuf::Message& command, RPC::ZeromqPub& pub) {
	RPC::Buffer cmdBuffer;
	RPC::ProtoBuf::serialize(command, cmdBuffer);
	std::pair<Result, uint64_t> result = globals.raft->replicate(cmdBuffer);
	if (result.first == Result::RETRY || result.first == Result::NOT_LEADER) {
		Protocol::Client::Error error;
		error.set_error_code(Protocol::Client::Error::NOT_LEADER);
		std::string leaderHint = globals.raft->getLeaderHint();
		if (!leaderHint.empty())
			error.set_leader_hint(leaderHint);
		rpc.returnError(error, pub);
	}
	return result;
}

Result ClientService::catchUpStateMachine(RPC::ZeromqServerRPC& rpc, RPC::ZeromqPub& pub) {
	std::pair<Result, uint64_t> result = globals.raft->getLastCommittedId();
	if (result.first == Result::RETRY || result.first == Result::NOT_LEADER) {
		Protocol::Client::Error error;
		error.set_error_code(Protocol::Client::Error::NOT_LEADER);
		std::string leaderHint = globals.raft->getLeaderHint();
		if (!leaderHint.empty())
			error.set_leader_hint(leaderHint);
		rpc.returnError(error, pub);
		return result.first;
	}
	globals.stateMachine->wait(result.second);
	return result.first;
}

bool ClientService::getResponse(RPC::ZeromqServerRPC& rpc, uint64_t entryId,
		const Protocol::Client::ExactlyOnceRPCInfo& rpcInfo,
		Protocol::Client::CommandResponse& response, RPC::ZeromqPub& pub) {
	globals.stateMachine->wait(entryId);
	bool ok = globals.stateMachine->getResponse(rpcInfo, response);
	if (!ok) {
		Protocol::Client::Error error;
		error.set_error_code(Protocol::Client::Error::SESSION_EXPIRED);
		rpc.returnError(error, pub);
		return false;
	}
	return true;
}

void ClientService::openSession(RPC::ZeromqServerRPC rpc, RPC::ZeromqPub& pub) {
	PRELUDE(OpenSession);
	Command command;
	command.set_nanoseconds_since_epoch(timeNanos());
	*command.mutable_open_session() = request;
	std::pair<Result, uint64_t> result = submit(rpc, command, pub);
	if (result.first != Result::SUCCESS)
		return;
	response.set_client_id(result.second);
	rpc.reply(response, pub);
}

void ClientService::getConfiguration(RPC::ZeromqServerRPC rpc, RPC::ZeromqPub& pub) {
	PRELUDE(GetConfiguration);
	Protocol::Raft::SimpleConfiguration configuration;
	uint64_t id;
	Result result = globals.raft->getConfiguration(configuration, id);
	if (result == Result::RETRY || result == Result::NOT_LEADER) {
		Protocol::Client::Error error;
		error.set_error_code(Protocol::Client::Error::NOT_LEADER);
		std::string leaderHint = globals.raft->getLeaderHint();
		if (!leaderHint.empty())
			error.set_leader_hint(leaderHint);
		rpc.returnError(error, pub);
		return;
	}
	response.set_id(id);
	for (auto it = configuration.servers().begin();
			it != configuration.servers().end(); ++it) {
		Protocol::Client::Server* server = response.add_servers();
		server->set_server_id(it->server_id());
		server->set_address(it->address());
	}
	rpc.reply(response, pub);
}

void ClientService::setConfiguration(RPC::ZeromqServerRPC rpc, RPC::ZeromqPub& pub) {
	PRELUDE(SetConfiguration);
	Protocol::Raft::SimpleConfiguration newConfiguration;
	for (auto it = request.new_servers().begin();
			it != request.new_servers().end(); ++it) {
		Protocol::Raft::Server* s = newConfiguration.add_servers();
		s->set_server_id(it->server_id());
		s->set_address(it->address());
	}
	Result result = globals.raft->setConfiguration(request.old_id(),
			newConfiguration);
	if (result == Result::SUCCESS) {
		response.mutable_ok();
	} else if (result == Result::RETRY || result == Result::NOT_LEADER) {
		Protocol::Client::Error error;
		error.set_error_code(Protocol::Client::Error::NOT_LEADER);
		std::string leaderHint = globals.raft->getLeaderHint();
		if (!leaderHint.empty())
			error.set_leader_hint(leaderHint);
		rpc.returnError(error, pub);
		return;
	} else if (result == Result::FAIL) {
		// TODO(ongaro): can't distinguish changed from bad
		response.mutable_configuration_changed();
	}
	rpc.reply(response, pub);
}

void ClientService::readOnlyTreeRPC(RPC::ZeromqServerRPC rpc, RPC::ZeromqPub& pub) {
	PRELUDE(ReadOnlyTree);
	if (catchUpStateMachine(rpc ,pub) != Result::SUCCESS)
		return;
	globals.stateMachine->readOnlyTreeRPC(request, response);
	rpc.reply(response ,pub);
}

void ClientService::readOnlyDataRPC(RPC::ZeromqServerRPC rpc, RPC::ZeromqPub& pub) {
	PRELUDE(ReadOnlyData);
	if (catchUpStateMachine(rpc ,pub) != Result::SUCCESS)
		return;
	globals.stateMachine->readOnlyDataRPC(request, response);
	rpc.reply(response ,pub);
}

void ClientService::readOnlyKDRPC(RPC::ZeromqServerRPC rpc, RPC::ZeromqPub& pub) {
	PRELUDE(ReadOnlyKD);
	if (catchUpStateMachine(rpc ,pub) != Result::SUCCESS)
		return;
	globals.stateMachine->readOnlyKDRPC(request, response);
	rpc.reply(response ,pub);
}

void ClientService::readWriteTreeRPC(RPC::ZeromqServerRPC rpc, RPC::ZeromqPub& pub) {
	PRELUDE(ReadWriteTree);
	Command command;
	command.set_nanoseconds_since_epoch(timeNanos());
	*command.mutable_tree() = request;
	std::pair<Result, uint64_t> result = submit(rpc, command ,pub);
	if (result.first != Result::SUCCESS)
		return;
	CommandResponse commandResponse;
	if (!getResponse(rpc, result.second, request.exactly_once(),
			commandResponse, pub)) {
		return;
	}
	rpc.reply(commandResponse.tree() ,pub);
}

void ClientService::readWriteDataRPC(RPC::ZeromqServerRPC rpc, RPC::ZeromqPub& pub) {
	PRELUDE(ReadWriteData);
	Command command;
	command.set_nanoseconds_since_epoch(timeNanos());
	*command.mutable_data() = request;
	std::pair<Result, uint64_t> result = submit(rpc, command ,pub);  //submit is to serialize data and replicate it with raft

	if (result.first != Result::SUCCESS)
		return;
	CommandResponse commandResponse;
	if (!getResponse(rpc, result.second, request.exactly_once(),
			commandResponse, pub))    //wait for the state machine
		return;
	rpc.reply(commandResponse.data() ,pub);
}

void ClientService::readWriteKDRPC(RPC::ZeromqServerRPC rpc, RPC::ZeromqPub& pub) {
	PRELUDE(ReadWriteKD);
	Command command;
	command.set_nanoseconds_since_epoch(timeNanos());
	*command.mutable_kd() = request;
	std::pair<Result, uint64_t> result = submit(rpc, command ,pub);
	if (result.first != Result::SUCCESS)
		return;
	CommandResponse commandResponse;
	if (!getResponse(rpc, result.second, request.exactly_once(),
			commandResponse ,pub))    //wait for the state machine
		return;
	rpc.reply(commandResponse.kd() ,pub);

}

}  // namespace LogCabin::Server
}  // namespace LogCabin
