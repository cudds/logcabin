#include "RPC/Service.h"

#include "build/Protocol/Client.pb.h"
#include "build/Protocol/Raft.pb.h"
#include "Core/Debug.h"
#include "Core/ProtoBuf.h"
#include "RPC/ClientRPC.h"
#include "Protocol/Common.h"
#include "RPC/ClientSession.h"
#include "RPC/zeromq/ZeromqPub.h"

#ifndef LOGCABIN_SERVER_CLIENTSERVICE_H
#define LOGCABIN_SERVER_CLIENTSERVICE_H

namespace LogCabin {
namespace Server {

// forward declaration
class Globals;
class Replication;

/**
 * This is LogCabin's application-facing RPC service. As some of these RPCs may
 * be long-running, this is intended to run under a RPC::ThreadDispatchService.
 */
class ClientService: public RPC::Service {
public:
	/// Constructor.
	explicit ClientService(Globals& globals, std::string smID);

	/// Destructor.
	~ClientService();

	void handleRPC(RPC::ZeromqServerRPC rpc);
	void handleRPC(RPC::ZeromqServerRPC rpc, RPC::ZeromqPub& pub);
	std::string getName() const;

private:

	////////// RPC handlers //////////
	void getSupportedRPCVersions(RPC::ZeromqServerRPC rpc, RPC::ZeromqPub& pub);
	void openSession(RPC::ZeromqServerRPC rpc, RPC::ZeromqPub& pub);
	void getConfiguration(RPC::ZeromqServerRPC rpc, RPC::ZeromqPub& pub);
	void setConfiguration(RPC::ZeromqServerRPC rpc, RPC::ZeromqPub& pub);
	void readOnlyTreeRPC(RPC::ZeromqServerRPC rpc, RPC::ZeromqPub& pub);
	void readWriteTreeRPC(RPC::ZeromqServerRPC rpc, RPC::ZeromqPub& pub);

	void readOnlyDataRPC(RPC::ZeromqServerRPC rpc, RPC::ZeromqPub& pub);
	void readWriteDataRPC(RPC::ZeromqServerRPC rpc, RPC::ZeromqPub& pub);
	void readOnlyKDRPC(RPC::ZeromqServerRPC rpc, RPC::ZeromqPub& pub);
	void readWriteKDRPC(RPC::ZeromqServerRPC rpc, RPC::ZeromqPub& pub);

	std::pair<RaftConsensus::ClientResult, uint64_t>
	submit(RPC::ZeromqServerRPC& rpc, const google::protobuf::Message& command, RPC::ZeromqPub& pub);

	RaftConsensus::ClientResult
	catchUpStateMachine(RPC::ZeromqServerRPC& rpc, RPC::ZeromqPub& pub);

	bool
	getResponse(RPC::ZeromqServerRPC& rpc, uint64_t entryId,
			const Protocol::Client::ExactlyOnceRPCInfo& rpcInfo,
			Protocol::Client::CommandResponse& response, RPC::ZeromqPub& pub);

	/**
	 * The LogCabin daemon's top-level objects.
	 */
	Globals& globals;
	std::string smID;

	// ClientService is non-copyable.
	ClientService(const ClientService&) = delete;
	ClientService& operator=(const ClientService&) = delete;
};
// class ClientService

}// namespace LogCabin::Server
}  // namespace LogCabin

#endif /* LOGCABIN_SERVER_CLIENTSERVICE_H */
