#include "build/Protocol/Raft.pb.h"
#include "Core/Debug.h"
#include "Core/ProtoBuf.h"
#include "RPC/ProtoBuf.h"
#include "RPC/ZeromqServerRPC.h"
#include "Server/RaftConsensus.h"
#include "Server/RaftService.h"
#include "Server/Globals.h"

namespace LogCabin {
namespace Server {

RaftService::RaftService(Globals& globals, std::string smID)
		: globals(globals), smID(smID){
}

RaftService::~RaftService() {
}

void RaftService::handleRPC(RPC::ZeromqServerRPC rpc) {
}

void RaftService::handleRPC(RPC::ZeromqServerRPC rpc, RPC::ZeromqPub& pub) {
	using Protocol::Raft::OpCode;

	// Call the appropriate RPC handler based on the request's opCode.
	switch (rpc.getOpCode()) {
		case OpCode::APPEND_ENTRIES:
			appendEntries(std::move(rpc), pub);
			break;
		case OpCode::APPEND_SNAPSHOT_CHUNK:
			appendSnapshotChunk(std::move(rpc), pub);
			break;
		case OpCode::REQUEST_VOTE:
			requestVote(std::move(rpc), pub);
			break;
		default:
			WARNING("Client sent request with bad op code (%u) to RaftService",
					rpc.getOpCode());
			rpc.rejectInvalidRequest(pub);
	}
}

std::string RaftService::getName() const {
	return smID + ":RaftService";
}

/**
 * Place this at the top of each RPC handler. Afterwards, 'request' will refer
 * to the protocol buffer for the request with all required fields set.
 * 'response' will be an empty protocol buffer for you to fill in the response.
 */
#define PRELUDE(rpcClass) \
    Protocol::Raft::rpcClass::Request request; \
    Protocol::Raft::rpcClass::Response response; \
    if (!rpc.getRequest(request)) \
        return; \
    /* TODO(ongaro): pass RPC into Raft so it can do this check instead */ \
    if (request.recipient_id() != globals.raft->serverId) { \
        rpc.closeSession(); \
        return; \
    }

////////// RPC handlers //////////
void RaftService::appendEntries(RPC::ZeromqServerRPC rpc, RPC::ZeromqPub& pub) {
	PRELUDE(AppendEntries);
	VERBOSE("AppendEntries:\n%s", Core::ProtoBuf::dumpString(request).c_str());
	globals.raft->handleAppendEntries(request, response);
	rpc.reply(response, pub);
}

void RaftService::appendSnapshotChunk(RPC::ZeromqServerRPC rpc, RPC::ZeromqPub& pub) {
	PRELUDE(AppendSnapshotChunk);
	VERBOSE("AppendSnapshotChunk:\n%s",
			Core::ProtoBuf::dumpString(request).c_str());
	globals.raft->handleAppendSnapshotChunk(request, response);
	rpc.reply(response, pub);
}

void RaftService::requestVote(RPC::ZeromqServerRPC rpc, RPC::ZeromqPub& pub) {
	PRELUDE(RequestVote);
	VERBOSE("RequestVote:\n%s", Core::ProtoBuf::dumpString(request).c_str());
	globals.raft->handleRequestVote(request, response);
	VERBOSE("raftservice:requestVote: starts to reply");
	rpc.reply(response, pub);
	VERBOSE("in raftservice:requestVote: rpc replied");
}

}  // namespace LogCabin::Server
}  // namespace LogCabin
