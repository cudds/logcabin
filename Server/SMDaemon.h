//written by ning gao in University of Colorado Boulder 
//<nigo9731@colorado.edu>#include <map>
#include <thread>
#include <vector>
#include "Core/Config.h"
#include "Server/Globals.h"
#include "RPC/zeromq/ZeromqDaemonRecv.h"
#include "logcabin/RPC/zeromq/ZeromqDaemonSend.h"
#ifndef LOGCABIN_SERVER_SMDAEMON_H
#define LOGCABIN_SERVER_SMDAEMON_H

namespace LogCabin {
namespace Server {

/*
 SMDaemon is a class to manage state machines where it receives create/delete state machines request on the port 61022
 when the SMDaemon thread starts up, it first starts the Router thread and then wait for RPC requests and global is the sole class for state machine module
 It maintains a map of state machine information and will be able to terminate all the instances in cluster whichever node receives the request.
 The mutex is used because Daemon thread and Spectrum thread may contend to create/delete state machines
 */
class SMDaemon {
public:
	struct sm_state {
		std::string smID;
		uint16_t serverID;
		std::vector<std::string> servers;
	};
	SMDaemon(std::shared_ptr<zmq::context_t> context);
	~SMDaemon();

	void startDaemon(const std::string configFile);
	void terminateDaemon();
	void createSM(sm_state smStates, std::string serverlists);
	void deleteSM(std::string smID);

	bool shouldExit;

private:
	Core::Config sharedConfig;
	std::map<std::string,
			std::tuple<std::shared_ptr<Server::Globals>, std::thread, sm_state> > SMs;
	std::thread zeromqRouter;
	std::shared_ptr<bool> terminate_router;
	RPC::ZeromqDaemonRecv receiver;
	std::string port;

private:
	void SMMainThread(Core::Config config,
			std::shared_ptr<Server::Globals> globals);
	//shared among daemon related threads
	std::shared_ptr<zmq::context_t> context;
	//protect SMs
	std::mutex mutex;
};

}  //namespace of Server
}  //namespace of LogCabin

#endif
