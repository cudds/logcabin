#include "SMDaemon.h"
#include "RPC/zeromq/ZeromqReceiver.h"
#include "Core/ThreadId.h"
#include "Core/Debug.h"
#include "Server/RaftConsensus.h"
#include "Core/ProtoBuf.h"
#include "build/Protocol/Daemon.pb.h"
#include "Core/StringUtil.h"
#include "logcabin/RPC/DaemonSendHelper.h"

namespace LogCabin {
namespace Server {

SMDaemon::SMDaemon(std::shared_ptr<zmq::context_t> context)
		: shouldExit(false), receiver(context), context(context), port("61022") {
}

SMDaemon::~SMDaemon() {
}

void SMDaemon::terminateDaemon() {
	std::unique_lock<std::mutex> lockGuard(mutex);  //allows recursive locking
	for (auto it = SMs.begin(); it != SMs.end(); ++it) {
		deleteSM(it->first);
	}

	if (terminate_router)
		*terminate_router = true;
	if (zeromqRouter.joinable()) {
		zeromqRouter.join();
		VERBOSE("router joined");
	}
}

void SMDaemon::startDaemon(const std::string configFile) {
	{
		std::unique_lock<std::mutex> lockGuard(mutex);
		sharedConfig.readFile(configFile);
		Core::ThreadId::setName("SMDaemon");
		Core::Debug::setLogPolicy( { { "Server", sharedConfig.read("logLevel") }, { "RPC", sharedConfig.read("logLevel") },  { "Storage", sharedConfig.read("logLevel") }});

		terminate_router = std::make_shared<bool>(false);
		zeromqRouter = std::thread(RPC::start_router,
				sharedConfig.read<std::string>("routerFrontPort"),
				sharedConfig.read<std::string>("routerBackPort"), terminate_router);
		port = sharedConfig.read<std::string>("daemonPort");
		receiver.bind(port);
	}

	int size;
	while (true) {
		void *message = receiver.wait(size, shouldExit);
		if (message == NULL)
			break;
		Protocol::Daemon::Request request;
		if (!request.ParseFromArray(static_cast<const char *>(message), size)) {
			WARNING("Missing fields in protocol buffer of type %s: %s",
					request.GetTypeName().c_str(), request.InitializationErrorString().c_str());
			continue;
		}
		//create the local sm and all the others
		if (request.kind() == Protocol::Daemon::Kind::CREATE) {
			sm_state smStates;
			smStates.smID = request.smid();
			smStates.serverID = (uint16_t) request.serverid();
			std::string serverlist;
			for (auto it = request.servers().begin(); it != request.servers().end(); ++it) {
				smStates.servers.push_back(*it);
				serverlist += *it + ";";
			}
			createSM(smStates, serverlist.substr(0, serverlist.length() - 1));
		} else {
			deleteSM(request.smid());
		}
		receiver.sendReady();
	}

}

void SMDaemon::SMMainThread(Core::Config config, std::shared_ptr<Server::Globals> globals) {
	Core::ThreadId::setName(config.read<std::string>("smID") + ":global");
	globals->config = config;
	globals->init();
	if (config.read<uint16_t>("serverID") == 1) {
		globals->raft->bootstrapConfiguration();
		NOTICE("Done bootstrapping configuration. Exiting.");
	}
	globals->run();
}

void SMDaemon::createSM(sm_state smStates, std::string serverlists) {
	std::cout << smStates.smID << " is created" << std::endl;
	std::unique_lock<std::mutex> lockGuard(mutex);
	auto it = SMs.find(smStates.smID);
	if (it == SMs.end()) {
		Core::Config smConfig;
		std::string selfAddress = smStates.servers[smStates.serverID - 1] + ":"
				+ sharedConfig.read<std::string>("routerFrontPort");
		smConfig.combine(sharedConfig, false);
		smConfig.set("smID", smStates.smID);
		smConfig.set("serverID", smStates.serverID);
		smConfig.set("maxThreads", sharedConfig.read<uint16_t>("maxThreads", 16));
		smConfig.set("selfAddress", selfAddress);
		smConfig.set("snapshotRatio", sharedConfig.read<uint64_t>("snapshotRatio", 10));
		smConfig.set("snapshotMinLogSize", sharedConfig.read<uint64_t>("snapshotMinLogSize", 1024));
		std::shared_ptr<Server::Globals> globals = std::make_shared<Server::Globals>();
		SMs[smStates.smID] = std::make_tuple(globals, std::thread(&SMDaemon::SMMainThread, this, smConfig, globals),
				smStates);
		//create sms in other machines
		if (smStates.serverID != 1)
			return;
		for (int i = 0; i < smStates.servers.size(); i++) {
			if (i + 1 == smStates.serverID)
				continue;
			std::cout << "forward createsm request to other nodes " << smStates.servers[i]
					<< std::endl;
			LogCabin::RPC::DaemonSendHelper sendHelper = LogCabin::RPC::DaemonSendHelper(context);
			sendHelper.send(LogCabin::RPC::DaemonSendHelper::Kind::CREATE, serverlists,
					smStates.smID, i + 1, port);
		}
	}
}

void SMDaemon::deleteSM(std::string smID) {
	std::cout << "delete " << smID << " is called" << std::endl;
	std::unique_lock<std::mutex> lockGuard(mutex);
	auto it = SMs.find(smID);
	if (it == SMs.end())
		return;
	std::cout << "sm found" << std::endl;
	std::get<0>(it->second)->eventLoop.breakRun = true;
	std::get<0>(it->second)->eventLoop.exit();
	if (std::get<1>(it->second).joinable())
		std::get<1>(it->second).join();
	std::get<0>(it->second).reset();
	Storage::FilesystemUtil::remove(sharedConfig.read<std::string>("storagePath") + "/"
							+ smID);
	if (std::get<2>(it->second).serverID != 1) {
		SMs.erase(it);
		return;
	}

	//delete sms in other machines
	std::string serverlist;
	for (int i = 0; i < std::get<2>(it->second).servers.size(); i++) {
		serverlist += std::get<2>(it->second).servers[i] + ";";
	}
	serverlist = serverlist.substr(0, serverlist.length() - 1);
	for (int i = 0; i < std::get<2>(it->second).servers.size(); i++) {
		std::cout << "forward deletesm request to other nodes "
				<< std::get<2>(it->second).servers[i] << std::endl;
		if (i + 1 == std::get<2>(it->second).serverID)
			continue;
		LogCabin::RPC::DaemonSendHelper sendHelper = LogCabin::RPC::DaemonSendHelper(context);
		sendHelper.send(LogCabin::RPC::DaemonSendHelper::Kind::DELETE, serverlist,
				std::get<2>(it->second).smID, i + 1, port);
	}
	SMs.erase(it);
}

}  //namespace of Server
}  //namespace of LogCabin
