#include "RPC/Service.h"
#include "RPC/zeromq/ZeromqPub.h"

#ifndef LOGCABIN_SERVER_RAFTSERVICE_H
#define LOGCABIN_SERVER_RAFTSERVICE_H

namespace LogCabin {
namespace Server {

// forward declaration
class Globals;
class ZeromqServerRPC;
class ZeromqPub;

// TODO(ongaro): doc
class RaftService: public RPC::Service {
public:
	explicit RaftService(Globals& globals, std::string smI);
	~RaftService();
	void handleRPC(RPC::ZeromqServerRPC rpc);
	void handleRPC(RPC::ZeromqServerRPC rpc, RPC::ZeromqPub& pub);
	std::string getName() const;

private:
	////////// RPC handlers //////////
	void requestVote(RPC::ZeromqServerRPC rpc, RPC::ZeromqPub& pub);
	void appendEntries(RPC::ZeromqServerRPC rpc, RPC::ZeromqPub& pub);
	void appendSnapshotChunk(RPC::ZeromqServerRPC rpc, RPC::ZeromqPub& pub);

	/**
	 * The LogCabin daemon's top-level objects.
	 */
	Globals& globals;
	std::string smID;

public:

	// RaftService is non-copyable.
	RaftService(const RaftService&) = delete;
	RaftService& operator=(const RaftService&) = delete;
};

}  // namespace LogCabin::Server
}  // namespace LogCabin

#endif /* LOGCABIN_SERVER_RAFTSERVICE_H */
