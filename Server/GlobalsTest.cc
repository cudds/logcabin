/* Copyright (c) 2012 Stanford University
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR(S) DISCLAIM ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL AUTHORS BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <gtest/gtest.h>

#include "Protocol/Common.h"
#include "RPC/Address.h"
#include "RPC/ZeromqServer.h"
#include "Server/Globals.h"

namespace LogCabin {
namespace Server {
namespace {

TEST(ServerGlobalsTest, basics) {
	std::shared_ptr<bool> terminate_router = std::make_shared<bool>(false);
	std::thread zeromqRouter = std::thread(LogCabin::RPC::start_router,
			"61023", "5555", terminate_router);

	Globals globals;
	globals.config.set("storageModule", "Memory");
	globals.config.set("uuid", "my-fake-uuid-123");
	globals.config.set("servers", "127.0.0.1");
	globals.config.set("smID", "sm1");
	globals.config.set("routerBackPort", "5555");
	globals.config.set("serverID", 1);
	globals.config.set("selfAddress", "128.138.244.47:61023");
	globals.config.set("storagePath", "/mnt/ramdisk");

	globals.init();
	globals.eventLoop.breakRun = true;
	globals.eventLoop.exit();
	globals.run();
	globals.exitProxy();
	if (terminate_router)
		*terminate_router = true;
	if (zeromqRouter.joinable()) {
		zeromqRouter.join();
		VERBOSE("router joined");
	}
}
//commented by ning
//these test is not needed because there's no server list provided now
/*
 TEST(ServerGlobalsTest, initNoServers) {
 Globals globals;
 globals.config.set("storageModule", "memory");
 globals.config.set("uuid", "my-fake-uuid-123");
 EXPECT_DEATH(globals.init(),
 "No server addresses specified");
 }

 TEST(ServerGlobalsTest, initEmptyServers) {
 Globals globals;
 globals.config.set("storageModule", "memory");
 globals.config.set("uuid", "my-fake-uuid-123");
 globals.config.set("servers", ";");
 EXPECT_DEATH(globals.init(),
 "invalid address");
 }

 TEST(ServerGlobalsTest, initAddressTaken) {

 //commented by ning
 //       Event::Loop eventLoop;
 //    RPC::Server server(eventLoop, 1);
 //    EXPECT_EQ("", server.bind(RPC::Address("127.0.0.1",
 //                                           Protocol::Common::DEFAULT_PORT)));

 Globals globals;
 globals.config.set("storageModule", "memory");
 globals.config.set("uuid", "my-fake-uuid-123");
 globals.config.set("servers", "127.0.0.1");
 EXPECT_DEATH(globals.init(),
 "in use");
 }


 TEST(ServerGlobalsTest, initBindToOneOnly) {
 Event::Loop eventLoop;
 //commented by ning
 //    RPC::Server server(eventLoop, 1);
 //    EXPECT_EQ("", server.bind(RPC::Address("127.0.0.1",
 //                                           Protocol::Common::DEFAULT_PORT)));
 RPC::ZeromqServer server;
 EXPECT_EQ("", server.bind("127.0.0.1:61023","sm1"));
 Globals globals;
 globals.config.set("storageModule", "memory");
 globals.config.set("uuid", "my-fake-uuid-123");
 globals.config.set("servers", "127.0.0.1:61023;127.0.0.1:61024");
 globals.init();
 }
 */

}// namespace LogCabin::Server::<anonymous>
}  // namespace LogCabin::Server
}  // namespace LogCabin
