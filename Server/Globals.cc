#include <signal.h>

#include "Core/Debug.h"
#include "Core/StringUtil.h"
#include "Protocol/Common.h"
#include "RPC/Server.h"
#include "Server/RaftService.h"
#include "Server/RaftConsensus.h"
#include "Server/ClientService.h"
#include "Server/Globals.h"
#include "Server/StateMachine.h"

namespace LogCabin {
namespace Server {

////////// Globals::SigIntHandler //////////

Globals::ExitHandler::ExitHandler(Event::Loop& eventLoop, int signalNumber)
		: Signal(eventLoop, signalNumber) {
}

void Globals::ExitHandler::handleSignalEvent() {
	NOTICE("%s: shutting down", strsignal(signalNumber));
	eventLoop.exit();
}

////////// Globals //////////

Globals::Globals()
		: config(), eventLoop(), sigIntHandler(eventLoop, SIGINT), sigTermHandler(
				eventLoop, SIGTERM), raft(), stateMachine(), raftService(), clientService()
		, zeromqServer()
		, zeromqProxy() {
}

Globals::~Globals() {
	// LogManager assumes it and its logs have no active users when it is
	// destroyed. Currently, the only user is clientService, and this is
	// guaranteed by clientService's destructor.
	shutIncomeProxy();
	zeromqServer.reset();
	stateMachine.reset();
	raft.reset();
	exitProxy();
}

void Globals::init() {
	context = std::make_shared<zmq::context_t>(1);
	if (!raft) {
		raft.reset(new RaftConsensus(*this));
	}

	if (!raftService) {
		raftService.reset(
				new RaftService(*this, config.read<std::string>("smID")));
	}

	if (!clientService) {
		clientService.reset(
				new ClientService(*this, config.read<std::string>("smID")));
	}

	startProxy(config.read<std::string>("smID"));

	if (!zeromqServer) {
		zeromqServer.reset(new RPC::ZeromqServer(context));
		eventLoop.addServer(zeromqServer);

		uint32_t maxThreads = config.read<uint16_t>("maxThreads", 16);
		zeromqServer->registerService(Protocol::Common::ServiceId::RAFT_SERVICE,
				raftService, maxThreads, config.read<std::string>("smID"));
		zeromqServer->registerService(
				Protocol::Common::ServiceId::CLIENT_SERVICE, clientService,
				maxThreads, config.read<std::string>("smID"));

		std::string error = "receiver address could not be bind";
		error = zeromqServer->connect(config.read<std::string>("smID"));
		assert(error == "");

		raft->serverId = config.read<uint16_t>("serverID");
		//serverAddress is recorded in Consensus base class and will be assigned to Server class in RaftConsensus.cc
		//right now it's connected to tcp://localhost:5555. For multiple state machines to work later, this might need to be changed.
		raft->serverAddress = config.read<std::string>("selfAddress");
		raft->smID = config.read<std::string>("smID");
		Core::Debug::processName = Core::StringUtil::format("%lu",
				raft->serverId);
		raft->init(
				config.read<std::string>("storagePath") + "/"
						+ config.read<std::string>("smID"));

		if (!stateMachine) {
			stateMachine.reset(new StateMachine(raft, config));
		}
	}

}

void Globals::startProxy(std::string smID) {
	terminate_proxy = std::make_shared<bool>(false);
	shutIncome_proxy = std::make_shared<bool>(false);
	zeromqProxy = std::thread(LogCabin::RPC::startProxy,
			config.read<std::string>("routerBackPort"), smID, terminate_proxy, shutIncome_proxy,
			context);
}

void Globals::shutIncomeProxy(){
	if(shutIncome_proxy){
		*shutIncome_proxy = true;
	}

}

void Globals::exitProxy() {
	if (terminate_proxy)
		*terminate_proxy = true;
	if (zeromqProxy.joinable()) {
		zeromqProxy.join();
	}
}
void Globals::run() {
	eventLoop.runForever();
}

}  // namespace LogCabin::Server
}  // namespace LogCabin
