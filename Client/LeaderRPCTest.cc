/* Copyright (c) 2012 Stanford University
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR(S) DISCLAIM ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL AUTHORS BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <gtest/gtest.h>
#include <thread>

#include "Client/LeaderRPC.h"
#include "Core/Debug.h"
#include "Core/ProtoBuf.h"
#include "Protocol/Common.h"
#include "RPC/ZeromqClientSession.h"
#include "RPC/ZeromqServer.h"

#include "RPC/ServiceMock.h"

namespace LogCabin {
namespace Client {
namespace {

using Protocol::Client::OpCode;

class ClientLeaderRPCTest: public ::testing::Test {
public:
	ClientLeaderRPCTest()
			: serverEventLoop(), service(), server(), serverThread(), leaderRPC(), request(), response(), expResponse() {
		terminate_router = std::make_shared<bool>(false);
		zeromqRouter = std::thread(LogCabin::RPC::start_router,
				std::string("61023"), std::string("5555"),
				terminate_router);
		context = std::make_shared<zmq::context_t>(1);
		terminate_proxy = std::make_shared<bool>(false);
		zeromqProxy = std::thread(LogCabin::RPC::start_proxy,
				std::string("5555"), std::string("sm1"),
				terminate_proxy, context);

		service = std::make_shared<RPC::ServiceMock>(context, "sm1");
		server.reset(new RPC::ZeromqServer(context));

		EXPECT_EQ("", server->connect("sm1"));
		server->registerService(Protocol::Common::ServiceId::CLIENT_SERVICE,
				service, 1, "sm1");
		serverEventLoop.addServer(server);
		RPC::Address address_a("localhost", 61023);
		leaderRPC.reset(new LeaderRPC(address_a, "sm1"));

		request.mutable_read()->set_path("foo");
		expResponse.set_status(Protocol::Client::Status::OK);
		expResponse.mutable_read()->set_contents("bar");

	}
	~ClientLeaderRPCTest() {
		serverEventLoop.breakRun = true;

		serverEventLoop.exit();
		if (serverThread.joinable())
			serverThread.join();
		*terminate_proxy = true;
		zeromqProxy.join();
		*terminate_router = true;
		zeromqRouter.join();
		server->close();
	}

	void init() {
		serverThread = std::thread(&Event::Loop::runForever, &serverEventLoop);
		sleep(1);
	}

	Event::Loop serverEventLoop;
	std::shared_ptr<RPC::ServiceMock> service;
	std::shared_ptr<zmq::context_t> context;
	std::shared_ptr<RPC::ZeromqServer> server;
	std::thread serverThread;
	std::unique_ptr<LeaderRPC> leaderRPC;
	Protocol::Client::ReadOnlyTree::Request request;
	Protocol::Client::ReadOnlyTree::Response response;
	Protocol::Client::ReadOnlyTree::Response expResponse;
	std::thread zeromqRouter;
	std::thread zeromqProxy;
	std::shared_ptr<bool> terminate_router;
	std::shared_ptr<bool> terminate_proxy;
};

// constructor and destructor tested adequately in tests for call()

TEST_F(ClientLeaderRPCTest, callOK) {

	init();
	service->reply(OpCode::READ_ONLY_TREE, request, expResponse);
	leaderRPC->call(OpCode::READ_ONLY_TREE, request, response);
	EXPECT_EQ(expResponse, response);
}

/*
 TEST_F(ClientLeaderRPCTest, callRPCFailed) {
 init();
 service->closeSession(OpCode::READ_ONLY_TREE, request);
 service->reply(OpCode::READ_ONLY_TREE, request, expResponse);
 leaderRPC->call(OpCode::READ_ONLY_TREE, request, response);
 EXPECT_EQ(expResponse, response);
 }
 */
// For service-specific error case,
// see handleServiceSpecificErrorNotLeader below.
TEST_F(ClientLeaderRPCTest, handleServiceSpecificErrorNotLeader) {
	init();
	Protocol::Client::Error error;
	error.set_error_code(Protocol::Client::Error::NOT_LEADER);

	// no hint
	service->serviceSpecificError(OpCode::READ_ONLY_TREE, request, error);

	// sucky hint
	error.set_leader_hint("localhost:61023");
	service->serviceSpecificError(OpCode::READ_ONLY_TREE, request, error);

	// ok, fine, let it through
	service->reply(OpCode::READ_ONLY_TREE, request, expResponse);

	leaderRPC->call(OpCode::READ_ONLY_TREE, request, response);
	EXPECT_EQ(expResponse, response);
}

/* timer not enabled
 TEST_F(ClientLeaderRPCTest, handleServiceSpecificErrorSessionExpired) {
 Protocol::Client::Error error;
 error.set_error_code(Protocol::Client::Error::SESSION_EXPIRED);

 leaderRPC->eventLoop.exit();
 leaderRPC->eventLoopThread.join();

 EXPECT_DEATH({
 leaderRPC->eventLoopThread = std::thread(&Event::Loop::runForever,
 &leaderRPC->eventLoop);
 init();
 service->serviceSpecificError(OpCode::READ_ONLY_TREE,
 request, error);
 leaderRPC->call(OpCode::READ_ONLY_TREE, request, response);
 },
 "Session expired");
 }
 */
// connect() tested adequately in tests for call()
TEST_F(ClientLeaderRPCTest, connectRandom) {
	// TODO(ongaro): This is hard to test without control of name resolution.
}

TEST_F(ClientLeaderRPCTest, connectHost) {
	init();
	//commented by ning
	//leaderRPC->connectHost("127.0.0.2:0", leaderRPC->leaderSession);
	//added by ning
	leaderRPC->connectHost("localhost:61023", "sm1", leaderRPC->leaderSession);
	EXPECT_EQ("Active session to localhost:61023 (resolved to 127.0.0.1:61023)",
			leaderRPC->leaderSession->toString());
}

}  // namespace LogCabin::Client::<anonymous>
}  // namespace LogCabin::Client
}  // namespace LogCabin
