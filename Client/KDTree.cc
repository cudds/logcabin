//written by Ning
#include "logcabin/Client/KDTree.h"
#include "Client/ClientImplBase.h"
#include "Client/ClientImpl.h"

namespace LogCabin {
namespace Client {

KDTree::KDTree(std::shared_ptr<ClientImplBase> clientImpl)
		: mutex(), clientImpl(clientImpl) {

}

KDTree::KDTree(const KDTree& other)
		: mutex(), clientImpl(other.getClientImp()) {

}

KDTree&
KDTree::operator=(const KDTree& other) {
	std::shared_ptr<ClientImplBase> clientImpLocal = other.getClientImp();
	std::unique_lock<std::mutex> lockGuard(mutex);
	clientImpl = clientImpLocal;
	return *this;

}
/*
 Result 
 KDTree::write(const std::string& key, const std::string& value)
 {
 std::shared_ptr<ClientImplBase> clientImplLocal = getClientImp();
 return clientImplLocal->writeData(key,value);
 }

 Result
 KDTree::read(const std::string& key, std::string& value) const
 {
 std::shared_ptr<ClientImplBase> clientImplLocal = getClientImp();
 return clientImplLocal->readData(key,value);
 }
 */

Result KDTree::insert(const twin& data) {
	std::shared_ptr<ClientImplBase> clientImplLocal = getClientImp();
	return clientImplLocal->insertKD(data.d[0], data.d[1]);
}

Result KDTree::erase(const twin& data) {
	std::shared_ptr<ClientImplBase> clientImplLocal = getClientImp();
	return clientImplLocal->eraseKD(data.d[0], data.d[1]);
}

Result KDTree::find_within_range(const twin& data, double range,
		std::vector<twin>& points) {
	std::shared_ptr<ClientImplBase> clientImplLocal = getClientImp();
	return clientImplLocal->findKD(data.d[0], data.d[1], range, points);
}

std::shared_ptr<ClientImplBase> KDTree::getClientImp() const {
	std::shared_ptr<ClientImplBase> ret;
	std::unique_lock<std::mutex> lockGuard(mutex);
	ret = clientImpl;
	return ret;
}

}
}
