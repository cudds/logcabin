#include "logcabin/Client/DataStore.h"
#include "Client/ClientImplBase.h"
#include "Client/ClientImpl.h"

namespace LogCabin {
namespace Client {

DataStore::DataStore(std::shared_ptr<ClientImplBase> clientImpl)
		: clientImpl(clientImpl) {

}

DataStore::DataStore(const DataStore& other)
		: clientImpl(other.clientImpl) {

}

DataStore&
DataStore::operator=(const DataStore& other) {
	clientImpl = other.clientImpl;
	return *this;

}

Result DataStore::write(const std::string& key, const std::string& value) {
	return clientImpl->writeData(key, value);
}

Result DataStore::read(const std::string& key, std::string& value) const {
	return clientImpl->readData(key, value);
}

}
}
