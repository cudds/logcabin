#include "logcabin/RPC/DaemonSendHelper.h"
#include "Core/StringUtil.h"
#include "logcabin/Client/Client.h"
#include <cassert>
#include <getopt.h>
#include <vector>
#include <fstream>
#include <thread>

using LogCabin::Client::Cluster;
using LogCabin::Client::Configuration;
using LogCabin::Client::ConfigurationResult;

namespace {
class OptionParser {
public:
	OptionParser(int& argc, char**& argv)
			: argc(argc), argv(argv), smID("sm1"), dport("61022"), rport("61023"), kind(1), ipFile() {
		while (true) {
			static struct option longOptions[] = { { "smID", required_argument,
			NULL, 's' }, { "daemon_port", required_argument, NULL, 'd' }, { "router_port",
					required_argument, NULL, 'r' }, { "kind", required_argument,
			NULL, 'k' }, { "ips", required_argument, NULL, 'i' }, { "help",
			no_argument, NULL, 'h' }, { 0, 0, 0, 0 } };
			int c = getopt_long(argc, argv, "s:d:r:k:i:h", longOptions, NULL);

			// Detect the end of the options.
			if (c == -1)
				break;

			switch (c) {
				case 'd':
					dport = optarg;
					break;
				case 'r':
					rport = optarg;
					break;
				case 's':
					smID = optarg;
					break;
				case 'k':
					kind = atoi(optarg);
					break;
				case 'i':
					ipFile = optarg;
					break;
				case 'h':
					usage();
					exit(0);
				default:
					usage();
					exit(1);
			}
		}
	}

	void usage() {
		std::cout << "Usage: " << argv[0] << " [options] <servers>" << std::endl;
		std::cout << "Options: " << std::endl;
		std::cout << "  -s, --smID <state machine ID> " << "The state machine to create "
				<< "(default: sm1)" << std::endl;
		std::cout << "  -d, --daemon_port <daemon port number> " << "The state machine to talk to "
				<< "(default: 61022)" << std::endl;
		std::cout << "  -r, --router_port <router port number> " << "The state machine to talk to "
				<< "(default: 61023)" << std::endl;
		std::cout << "  -k --kind <create/delete>" << "1 means create, 0 means delete" << std::endl;
		std::cout << "  -i --ips <ip file>" << "path to file, which stores a list of ip addresses"
				<< std::endl;
		std::cout << "  -h, --help              " << "Print this usage information" << std::endl;
	}

	int& argc;
	char**& argv;
	std::string dport;
	std::string rport;
	std::string smID;
	std::string ipFile;
	int kind;
};

}

void printConfiguration(const std::pair<uint64_t, Configuration>& configuration) {
	std::cout << "Configuration " << configuration.first << ":" << std::endl;
	for (auto it = configuration.second.begin(); it != configuration.second.end(); ++it) {
		std::cout << "- " << it->first << ": " << it->second << std::endl;
	}
	std::cout << std::endl;
}

bool reconfig(std::vector<std::string> servers, std::string leader, std::string smID) {
	Cluster cluster(leader, smID);
	std::pair<uint64_t, Configuration> configuration = cluster.getConfiguration();
	printConfiguration(configuration);
	uint64_t id = configuration.first;
	Configuration serversConfig;
	for (uint64_t i = 0; i < servers.size(); ++i)
		serversConfig.emplace_back(i + 1, servers.at(i));
	ConfigurationResult result = cluster.setConfiguration(id, serversConfig);

	std::cout << "Reconfiguration ";
	if (result.status == ConfigurationResult::OK) {
		std::cout << "OK" << std::endl;
	} else if (result.status == ConfigurationResult::CHANGED) {
		std::cout << "CHANGED" << std::endl;
	} else if (result.status == ConfigurationResult::BAD) {
		std::cout << "BAD SERVERS:" << std::endl;
		for (auto it = result.badServers.begin(); it != result.badServers.end(); ++it) {
			std::cout << "- " << it->first << ": " << it->second << std::endl;
		}
	}
	printConfiguration(cluster.getConfiguration());

	if (result.status == ConfigurationResult::OK)
		return true;
	else
		return false;
}

int main(int argc, char** argv) {
	OptionParser options(argc, argv);
	std::shared_ptr<zmq::context_t> context = std::make_shared<zmq::context_t>(1);
	std::string ips;
	std::vector<std::string> ipVector;
	std::ifstream infile(options.ipFile);
	std::string line;
	while (std::getline(infile, line)) {
		std::string currentIP = LogCabin::Core::StringUtil::trimSpace(line);
		ips += currentIP + ";";
		ipVector.push_back(currentIP + ":" + options.rport);
	}

	LogCabin::RPC::DaemonSendHelper sendHelper = LogCabin::RPC::DaemonSendHelper(context);
	sendHelper.send(
			(options.kind == 1 ?
					LogCabin::RPC::DaemonSendHelper::Kind::CREATE :
					LogCabin::RPC::DaemonSendHelper::Kind::DELETE), ips.substr(0, ips.size() - 1),
			options.smID, 1, options.dport);
	if (options.kind == 1) {
		while(!reconfig(ipVector, ipVector[0], options.smID)){
			std::this_thread::sleep_for(std::chrono::milliseconds(1000));
		}
	}
	return 0;
}
