//written by ning gao in University of Colorado Boulder 
//<nigo9731@colorado.edu>

#if __GNUC__ >= 4 && __GNUC_MINOR__ >= 5
#include <atomic>
#else
#include <cstdatomic>
#endif
#include <cassert>
#include <ctime>
#include <getopt.h>
#include <iostream>
#include <thread>
#include <unistd.h>

#include "logcabin/Client/Client.h"

namespace {

using LogCabin::Client::Cluster;
using LogCabin::Client::Result;
using LogCabin::Client::Status;
using LogCabin::Client::DataStore;
using LogCabin::Client::KDTree;
using LogCabin::Client::twin;

/**
 * Parses argv for the main function.
 */
class OptionParser {
public:
	OptionParser(int& argc, char**& argv)
			: argc(argc), argv(argv), cluster("logcabin:61023"), size(1024), writers(
					1), totalWrites(1000), timeout(30) {
		while (true) {
			static struct option longOptions[] = { { "cluster",
					required_argument, NULL, 'c' }, { "smID", required_argument,
					NULL, 'm' }, { "help", no_argument, NULL, 'h' }, { "size",
					required_argument, NULL, 's' }, { "threads",
					required_argument, NULL, 't' }, { "timeout",
					required_argument, NULL, 'd' }, { "writes",
					required_argument, NULL, 'w' }, { 0, 0, 0, 0 } };
			int c = getopt_long(argc, argv, "c:m:hs:t:w:", longOptions, NULL);

			// Detect the end of the options.
			if (c == -1)
				break;

			switch (c) {
				case 'c':
					cluster = optarg;
					break;
				case 'm':
					smID = optarg;
					break;
				case 'd':
					timeout = uint64_t(atol(optarg));
					break;
				case 'h':
					usage();
					exit(0);
				case 's':
					size = uint64_t(atol(optarg));
					break;
				case 't':
					writers = uint64_t(atol(optarg));
					break;
				case 'w':
					totalWrites = uint64_t(atol(optarg));
					break;
				case '?':
				default:
					// getopt_long already printed an error message.
					usage();
					exit(1);
			}
		}
	}

	void usage() {
		std::cout << "Usage: " << argv[0] << " [options]" << std::endl;
		std::cout << std::endl;
		std::cout << "Writes repeatedly to LogCabin. Stops once it reaches "
				<< "the given number of " << std::endl;
		std::cout << "writes or the timeout, whichever comes " << "first."
				<< std::endl;
		std::cout << std::endl;
		std::cout << "Options: " << std::endl;
		std::cout << "  -c, --cluster <address> "
				<< "The network address of the LogCabin cluster " << std::endl;
		std::cout << "                          " << "[default: logcabin:61023]"
				<< std::endl;
		std::cout << "  -m smID" << std::endl;
		std::cout << "  -h, --help              "
				<< "Print this usage information" << std::endl;
		std::cout << "  --size <bytes>          "
				<< "Size of value in each write [default: 1024]" << std::endl;
		std::cout << "  --threads <num>         "
				<< "Number of concurrent writers [default: 1]" << std::endl;
		std::cout << "  --timeout <seconds>     "
				<< "Seconds after which to exit [default: 30]" << std::endl;
		std::cout << "  --writes <num>          "
				<< "Number of total writes [default: 1000]" << std::endl;
	}

	int& argc;
	char**& argv;
	std::string cluster;
	uint64_t size;
	uint64_t writers;
	uint64_t totalWrites;
	uint64_t timeout;
	std::string smID;
};

/**
 * The main function for a single client thread.
 * \param id
 *      Unique ID for this thread, counting from 0.
 * \param options
 *      Arguments describing benchmark.
 * \param tree
 *      Interface to LogCabin.
 * \param key
 *      Key to write repeatedly.
 * \param value
 *      Value to write at key repeatedly.
 * \param exit
 *      When this becomes true, this thread should exit.
 * \param[out] writesDone
 *      The number of writes this thread has completed.
 */
void writeThreadMain(uint64_t id, const OptionParser& options, KDTree tree,
		std::atomic<bool>& exit, uint64_t& writesDone) {
	uint64_t numWrites = options.totalWrites / options.writers;
	// assign any odd leftover writes in a balanced way
	if (options.totalWrites - numWrites * options.writers > id)
		numWrites += 1;
	std::string valuewrite;
	for (uint64_t i = 0; i < numWrites; ++i) {
		if (exit)
			break;
		//struct timespec nown;
		//clock_gettime(CLOCK_REALTIME, &nown);
		//std::cout<<nown.tv_sec<<"."<<nown.tv_nsec / 1000<<" "<<" :thread "<<id<<" send a rpc call"<<std::endl;
		tree.insert(twin(i, i));
		//        clock_gettime(CLOCK_REALTIME, &nown);
		//std::cout<<nown.tv_sec<<"."<<nown.tv_nsec / 1000<<" "<<" :thread "<<id<<" get a response"<<std::endl;
		writesDone = i + 1;
	}
}

/**
 * Return the time since the Unix epoch in nanoseconds.
 */
uint64_t timeNanos() {
	struct timespec now;
	int r = clock_gettime(CLOCK_REALTIME, &now);
	assert(r == 0);
	return uint64_t(now.tv_sec) * 1000 * 1000 * 1000 + now.tv_nsec;
}

/**
 * Main function for the timer thread, whose job is to wait until a particular
 * timeout elapses and then set 'exit' to true.
 * \param timeout
 *      Seconds to wait before setting exit to true.
 * \param[in,out] exit
 *      If this is set to true from another thread, the timer thread will exit
 *      soonish. Also, if the timeout elapses, the timer thread will set this
 *      to true and exit.
 */
void timerThreadMain(uint64_t timeout, std::atomic<bool>& exit) {
	uint64_t start = timeNanos();
	while (!exit) {
		usleep(50 * 1000);
		if ((timeNanos() - start) > timeout * 1000 * 1000 * 1000) {
			exit = true;
		}
	}
}

}  // anonymous namespace

int main(int argc, char** argv) {
	OptionParser options(argc, argv);
	//commented by ning
	//Cluster cluster = Cluster(options.cluster);
	//added by ning
	Cluster cluster(options.cluster, options.smID);
	KDTree tree = cluster.getKDTree();

	uint64_t startNanos = timeNanos();
	std::atomic<bool> exit(false);
	std::vector<uint64_t> writesDonePerThread(options.writers);
	uint64_t totalWritesDone = 0;
	std::vector<std::thread> threads;
	std::thread timer(timerThreadMain, options.timeout, std::ref(exit));
	for (uint64_t i = 0; i < options.writers; ++i) {
		threads.emplace_back(writeThreadMain, i, std::ref(options), tree,
				std::ref(exit), std::ref(writesDonePerThread.at(i)));
	}
	for (uint64_t i = 0; i < options.writers; ++i) {
		threads.at(i).join();
		totalWritesDone += writesDonePerThread.at(i);
	}
	uint64_t endNanos = timeNanos();
	exit = true;
	timer.join();

	std::cout << "Benchmark took "
			<< static_cast<double>(endNanos - startNanos) / 1e6
			<< " ms to write " << totalWritesDone << " objects" << std::endl;
	return 0;
}
