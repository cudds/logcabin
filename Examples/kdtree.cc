//written by Ning Gao
//University of Colorado Boulder
//used to test the state machine after replacing the tree api to a key-value data structure

#include <cassert>
#include <getopt.h>
#include <iostream>

#include "logcabin/Client/Client.h"

//added by ning
#include "Core/Debug.h"

namespace {

using LogCabin::Client::Cluster;
using LogCabin::Client::Tree;
using LogCabin::Client::DataStore;
using LogCabin::Client::KDTree;
using LogCabin::Client::twin;

/**
 * Parses argv for the main function.
 */
class OptionParser {
public:
	OptionParser(int& argc, char**& argv)
			: argc(argc), argv(argv), cluster("128.138.207.162:61023") {
		while (true) {
			static struct option longOptions[] = { { "cluster",
					required_argument, NULL, 'c' }, { "state",
					required_argument, NULL, 's' }, { "help", no_argument, NULL,
					'h' }, { 0, 0, 0, 0 } };
			int c = getopt_long(argc, argv, "c:s:h", longOptions, NULL);

			// Detect the end of the options.
			if (c == -1)
				break;

			switch (c) {
				case 'c':
					cluster = optarg;
					break;
				case 's':
					smID = optarg;
					break;
				case 'h':
					usage();
					exit(0);
				case '?':
				default:
					// getopt_long already printed an error message.
					usage();
					exit(1);
			}
		}
	}

	void usage() {
		std::cout << "Usage: " << argv[0] << " [options] <servers>"
				<< std::endl;
		std::cout << "Options: " << std::endl;
		std::cout << "  -c, --cluster <address> "
				<< "The network address of the LogCabin cluster "
				<< "(default: logcabin:61023)" << std::endl;
		std::cout << "  -s, --state <state machine ID> "
				<< "The state machine to talk to " << "(dedault: sm1)"
				<< std::endl;
		std::cout << "  -h, --help              "
				<< "Print this usage information" << std::endl;
	}

	int& argc;
	char**& argv;
	std::string cluster;
	std::string smID;
};

}  // anonymous namespace

int main(int argc, char** argv) {
	//LogCabin::Core::Debug::setLogPolicy({{"Server", "VERBOSE"},{"RPC", "VERBOSE"},{"Client", "VERBOSE"}});
	OptionParser options(argc, argv);
	Cluster cluster(options.cluster, options.smID);
	KDTree tree = cluster.getKDTree();
	tree.insert(twin(1, 1));
	tree.insert(twin(2, 2));
	std::vector<twin> result;
	tree.find_within_range(twin(1, 0.5), 1, result);
	for (auto it = result.begin(); it != result.end(); ++it)
		std::cout << (*it)[0] << " " << (*it)[1] << std::endl;

	return 0;
}
