#include "Core/Debug.h"
#include "Storage/LogFactory.h"
#include "Storage/MemoryLog.h"
#include "Storage/SimpleFileLog.h"
#include "Storage/SegmentedLog.h"

namespace LogCabin {
namespace Storage {
namespace LogFactory {

std::unique_ptr<Log> makeLog(const Core::Config& config, const FilesystemUtil::File& parentDir) {
	std::string module = config.read<std::string>("storageModule", "FileSystem");
	std::unique_ptr<Log> log;
	if (module == "Memory") {
		log.reset(new MemoryLog());
	} else if (module == "FileSystem") {
		log.reset(new SimpleFileLog(parentDir));
	} else if (module == "Segmented") {
		log.reset(new SegmentedLog(parentDir, SegmentedLog::Encoding::BINARY, config));
	} else {
		PANIC("Unknown storage module from config file: %s", module.c_str());
	}
	return log;
}

}  // namespace LogCabin::Storage::LogFactory
}  // namespace LogCabin::Storage
}  // namespace LogCabin
