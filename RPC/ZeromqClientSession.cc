#include <assert.h>
#include <string.h>
#include <unistd.h>

#include "Core/Debug.h"
#include "Core/StringUtil.h"
#include "RPC/ZeromqClientSession.h"
#include "RPC/ZeromqClientRPC.h"

namespace LogCabin {
namespace RPC {

ZeromqClientSession::Response::Response()
		: status(Response::WAITING), reply() {
}
////////// ZeromqClientSession //////////

ZeromqClientSession::ZeromqClientSession(const Address& address_, std::string smID)
		: address(address_), mutex(), sender(), nextMessageId(1){
	//create a connection
	sender.reset(new ZeromqSender(smID));
	bool returnVal = sender->connect(address.getZeromqString());
	if (!returnVal) {
		errorMessage = "Failed to connect socket to " + address.toString();
		return;
	}
}

std::shared_ptr<ZeromqClientSession> ZeromqClientSession::makeSession(const Address& address,
																		std::string smID) {
	std::shared_ptr<ZeromqClientSession> session(new ZeromqClientSession(address, smID));
	return session;
}

std::string ZeromqClientSession::getErrorMessage() const {
	std::unique_lock<std::mutex> mutexGuard(mutex);
	return errorMessage;
}

void ZeromqClientSession::sendRequest(ZeromqClientRPC *rpc, Buffer request) {
	uint64_t messageId;
	{
		std::unique_lock<std::mutex> mutexGuard(mutex);
		messageId = nextMessageId;
		++nextMessageId;
		responses[messageId] = new Response();
		if (sender)
			sender->sendMessage(messageId, request.getData(), request.getLength());
	}
	rpc->responseToken = messageId;
}

void ZeromqClientSession::cancel(ZeromqClientRPC& rpc) {
	// There are two ways to cancel an RPC:
	// 1. If there's some thread currently blocked in wait(), this method marks
	//    the Response's status as CANCELED, and wait() will delete it later.
	// 2. If there's no thread currently blocked in wait(), the Response is
	//    deleted entirely.
	std::unique_lock<std::mutex> mutexGuard(mutex);
	auto it = responses.find(rpc.responseToken);
	if (it == responses.end())
		return;
	Response* response = it->second;

	response->status = Response::CANCELED;
	delete response;
	responses.erase(it);
}

void ZeromqClientSession::update(ZeromqClientRPC& rpc) {
	std::unique_lock<std::mutex> mutexGuard(mutex);
	auto it = responses.find(rpc.responseToken);
	if (it == responses.end()) {
		// RPC was cancelled, fields set already
		assert(rpc.ready);
		return;
	}
	Response* response = it->second;
	if (response->status == Response::HAS_REPLY) {
		rpc.reply = std::move(response->reply);
	} else if (!errorMessage.empty()) {
		rpc.errorMessage = errorMessage;
	} else {
		// If the RPC was canceled, then it'd be marked ready and update()
		// wouldn't be called again.
		assert(response->status != Response::CANCELED);
		return;  // not ready
	}
	rpc.ready = true;
	rpc.session.reset();

	delete response;
	responses.erase(it);
}

std::string ZeromqClientSession::toString() const {
	std::string error = getErrorMessage();
	if (error.empty()) {
		return "Active session to " + address.toString();
	} else {
		// error will already include the server's address.
		return "Closed session: " + error;
	}
}

void ZeromqClientSession::wait(const ZeromqClientRPC& rpc, int maxTries) {
	std::unique_lock < std::mutex > mutexGuard(mutex);
	int tries = 0;
	while (true) {
		if(tries++ >= maxTries){
			return;
		}
		//temporarily release the lock so that cancel may be called
		mutexGuard.unlock();
		usleep(20);
		mutexGuard.lock();
		auto it = responses.find(rpc.responseToken);
		if (it == responses.end())  // canceled or already updated
			return;

		Response* response = it->second;
		if (response->status == Response::HAS_REPLY) {
			return;  // RPC has completed
		} else if (response->status == Response::CANCELED) {
			// RPC was cancelled, finish cleaning up
			delete response;
			responses.erase(it);
			return;
		} else if (!errorMessage.empty())
			return;  // session has error
		else if (response->status == Response::WAITING) {
			//codes to receive one message from sender
			int size;
			uint64_t messageId;
			void *responseArray = sender->nonblock_wait(size, messageId);
			if (responseArray == NULL || responses.find(rpc.responseToken) == responses.end())
				continue;
			if (responses.find(messageId) == responses.end())
				continue;
			Buffer buffer;
			buffer.setData(responseArray, size, RPC::Buffer::deleteArrayFn<void*>);
			responses[messageId]->reply = std::move(buffer);
			responses[messageId]->status = Response::HAS_REPLY;
		}

	}
}

}  // namespace LogCabin::RPC
}  // namespace LogCabin
