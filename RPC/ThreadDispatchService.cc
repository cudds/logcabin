#include <assert.h>

#include "Core/StringUtil.h"
#include "Core/ThreadId.h"
#include "RPC/ThreadDispatchService.h"

namespace LogCabin {
namespace RPC {

ThreadDispatchService::ThreadDispatchService(
		std::shared_ptr<Service> threadSafeService, uint32_t minThreads,
		uint32_t maxThreads, std::shared_ptr<zmq::context_t> context, std::string smID)
		: threadSafeService(threadSafeService), maxThreads(maxThreads), mutex(), threads(),
		  context(context), smID(smID),
		  numFreeWorkers(0), conditionVariable(), exit(false), rpcQueue() {
	assert(context!=NULL);
	assert(minThreads <= maxThreads);
	assert(0 < maxThreads);
	for (uint32_t i = 0; i < minThreads; ++i)
		threads.emplace_back(&ThreadDispatchService::workerMain, this);
}
void ThreadDispatchService::handleRPC(ZeromqServerRPC serverRPC, ZeromqPub& pub){

}

ThreadDispatchService::~ThreadDispatchService() {
	// Signal the threads to exit.
	{
		std::unique_lock<std::mutex> lockGuard(mutex);
		exit = true;
	}
	conditionVariable.notify_all();
	NOTICE("reclaiming dispatching threads");

	// Join the threads.
	while (!threads.empty()) {
		while(!threads.back().joinable()){
			conditionVariable.notify_all();
		}
		NOTICE("joining");
		threads.back().join();
		threads.pop_back();
	}
	NOTICE("dispatching threads reclaimed");
	// Close the sessions of any remaining RPCs that didn't get processed.
	while (!rpcQueue.empty()) {
		rpcQueue.front().closeSession();
		rpcQueue.pop();
	}
}

void ThreadDispatchService::handleRPC(ZeromqServerRPC serverRPC) {
	std::unique_lock<std::mutex> lockGuard(mutex);
	assert(!exit);
	rpcQueue.push(std::move(serverRPC));
	if (numFreeWorkers == 0 && threads.size() < maxThreads)
		threads.emplace_back(&ThreadDispatchService::workerMain, this);
	conditionVariable.notify_one();
}

std::string ThreadDispatchService::getName() const {
	return threadSafeService->getName();
}

int ThreadDispatchService::numOfThreads() {
	return threads.size();
}

void ThreadDispatchService::workerMain() {
	Core::ThreadId::setName(
			Core::StringUtil::format("%s(%lu)",
					threadSafeService->getName().c_str(),
					Core::ThreadId::getId()));
	NOTICE("ThreadDispatchService tid is %lu", gettid());
	ZeromqPub pub(context);
	std::string error = pub.connect(smID);
	assert(error=="");
	while (true) {
		ZeromqServerRPC rpc;
		{
			std::unique_lock<std::mutex> lockGuard(mutex);
			++numFreeWorkers;
			while (!exit && rpcQueue.empty())
			{
				conditionVariable.wait(lockGuard);
			}
			--numFreeWorkers;
			if (exit) {
				NOTICE("ThreadDispatchService tid %lu is reclaimed",gettid());
				return;
			}
			rpc = std::move(rpcQueue.front());
			rpcQueue.pop();
		}
		threadSafeService->handleRPC(std::move(rpc), pub);
	}
}

}  // namespace LogCabin::RPC
}  // namespace LogCabin
