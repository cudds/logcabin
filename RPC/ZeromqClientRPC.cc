#include "Core/Debug.h"
#include "Core/Mutex.h"
#include "RPC/Protocol.h"
#include "RPC/ZeromqClientRPC.h"
#include "RPC/ZeromqClientSession.h"
#include "RPC/ProtoBuf.h"

namespace LogCabin {
namespace RPC {

using RPC::Protocol::RequestHeaderPrefix;
using RPC::Protocol::RequestHeaderVersion1;
using RPC::Protocol::ResponseHeaderPrefix;
using RPC::Protocol::ResponseHeaderVersion1;
typedef RPC::Protocol::Status ProtocolStatus;

ZeromqClientRPC::ZeromqClientRPC()
		: responseToken(~0UL), reply(), errorMessage(), ready(false) {
}

ZeromqClientRPC::ZeromqClientRPC(
		std::shared_ptr<RPC::ZeromqClientSession> session_, uint16_t service,
		uint8_t serviceSpecificErrorVersion, uint16_t opCode,
		const google::protobuf::Message& request)
		: ready(false), responseToken(~0UL), session(session_), errorMessage(), reply() {
	//Serialize the request into a Buffer
	Buffer requestBuffer;
	ProtoBuf::serialize(request, requestBuffer, sizeof(RequestHeaderVersion1));  //the third argument is # of the bytes to skip but space is allocated
	auto& requestHeader =
			*static_cast<RequestHeaderVersion1*>(requestBuffer.getData());
	requestHeader.prefix.version = 1;
	requestHeader.prefix.toBigEndian();
	requestHeader.service = service;
	requestHeader.serviceSpecificErrorVersion = serviceSpecificErrorVersion;
	requestHeader.opCode = opCode;
	requestHeader.toBigEndian();

	// Send the request to the server
	assert(session_);  // makes debugging more obvious for somewhat common error
	session_->sendRequest(const_cast<ZeromqClientRPC*>(this),
			std::move(requestBuffer));  //Buffer is not copyable, so use move
}

ZeromqClientRPC::ZeromqClientRPC(ZeromqClientRPC&& other)
		: mutex(), session(std::move(other.session)), responseToken(
				std::move(other.responseToken)), ready(std::move(other.ready)), reply(
				std::move(other.reply)), errorMessage(
				std::move(other.errorMessage)) {
}

ZeromqClientRPC::~ZeromqClientRPC() {
}

ZeromqClientRPC&
ZeromqClientRPC::operator=(ZeromqClientRPC&& other) {
	std::unique_lock<std::mutex> mutexGuard(mutex);
	session = std::move(other.session);
	responseToken = std::move(other.responseToken);
	ready = std::move(other.ready);
	reply = std::move(other.reply);
	errorMessage = std::move(other.errorMessage);
	return *this;
}

void ZeromqClientRPC::cancel() {
	std::unique_lock<std::mutex> mutexGuard(mutex);
	if (ready) {
		return;
	}
	if (session)
		session->cancel(*this);
	ready = true;
	session.reset();
	reply.reset();
	errorMessage = "RPC canceled by user";
}

std::string ZeromqClientRPC::getErrorMessage() const {
	return errorMessage;
}

ZeromqClientRPC::Status ZeromqClientRPC::nonBlockWaitForReply(google::protobuf::Message* response,
                			google::protobuf::Message* serviceSpecificError){
	Buffer responseBuffer;
	{
		std::unique_lock<std::mutex> mutexGuard(mutex);
		if (!ready) {
			if (session) {
				{
					// release the mutex while calling wait()
					Core::MutexUnlock<std::mutex> unlockGuard(mutexGuard);
					session->wait(*this, 1000);
				}
				update();
			} else {
				ready = true;
				errorMessage =
						"This RPC was never associated with a Client Session.";
			}
		}
	}
	if(!ready){
		return Status::RPC_FAILED;
	}

	if (!errorMessage.empty())
		return Status::RPC_FAILED;

	// Extract the response's status field.
	if (reply.getLength() < sizeof(ResponseHeaderPrefix)) {
		PANIC(
				"The response from the server was too short to be valid. " "This probably indicates network or memory corruption.");
	}
	ResponseHeaderPrefix responseHeaderPrefix =
			*static_cast<const ResponseHeaderPrefix*>(reply.getData());
	responseHeaderPrefix.fromBigEndian();
	if (responseHeaderPrefix.status == ProtocolStatus::INVALID_VERSION) {
		// The server doesn't understand this version of the header
		// protocol. Since this library only runs version 1 of the
		// protocol, this shouldn't happen if servers continue supporting
		// version 1.
		PANIC(
				"This client is too old to talk to the server. " "You'll need to update your client library.");
	}

	if (reply.getLength() < sizeof(ResponseHeaderVersion1)) {
		PANIC(
				"The response from the server was too short to be valid. " "This probably indicates network or memory corruption.");
	}
	ResponseHeaderVersion1 responseHeader =
			*static_cast<const ResponseHeaderVersion1*>(reply.getData());
	responseHeader.fromBigEndian();

	switch (responseHeader.prefix.status) {

		// The RPC succeeded. Parse the response into a protocol buffer.
		case ProtocolStatus::OK:
			if (response != NULL
					&& !RPC::ProtoBuf::parse(reply, *response,
							sizeof(responseHeader))) {
				PANIC(
						"Could not parse the protocol buffer out of the server " "response");
			}
			return Status::OK;

			// The RPC failed in a service-specific way. Parse the response into a
			// protocol buffer.
		case ProtocolStatus::SERVICE_SPECIFIC_ERROR:
			if (serviceSpecificError != NULL
					&& !RPC::ProtoBuf::parse(reply, *serviceSpecificError,
							sizeof(responseHeader))) {
				PANIC(
						"Could not parse the protocol buffer out of the " "service-specific error details");
			}
			return Status::SERVICE_SPECIFIC_ERROR;

			// The server does not have the requested service.
		case ProtocolStatus::INVALID_SERVICE:
			PANIC("The server is not running the requested service.");

			// The server disliked our request. This shouldn't happen because
			// the higher layers of software were supposed to negotiate an RPC
			// protocol version.
		case ProtocolStatus::INVALID_REQUEST:
			PANIC(
					"The server found the request to be invalid. This " "indicates a bug in the client or server in negotiating " "which RPCs the client may legally send to the server.");

		default:
			// The server shouldn't reply back with status codes we don't
			// understand. That's why we gave it a version number in the
			// request header.
			PANIC(
					"Unknown status %u returned from server after sending it " "protocol version 1 in the request header. This probably " "indicates a bug in the server.",
					uint32_t(responseHeader.prefix.status));
	}

}
/*
ZeromqClientRPC::Status ZeromqClientRPC::waitForReply(
		google::protobuf::Message* response,
		google::protobuf::Message* serviceSpecificError) {
	Buffer responseBuffer;
	{
		std::unique_lock<std::mutex> mutexGuard(mutex);
		if (!ready) {
			if (session) {
				{
					// release the mutex while calling wait()
					Core::MutexUnlock<std::mutex> unlockGuard(mutexGuard);
					session->wait(*this);
				}
				update();
			} else {
				ready = true;
				errorMessage =
						"This RPC was never associated with a Client Session.";
			}
		}
	}

	if (!errorMessage.empty())
		return Status::RPC_FAILED;

	// Extract the response's status field.
	if (reply.getLength() < sizeof(ResponseHeaderPrefix)) {
		PANIC(
				"The response from the server was too short to be valid. " "This probably indicates network or memory corruption.");
	}
	ResponseHeaderPrefix responseHeaderPrefix =
			*static_cast<const ResponseHeaderPrefix*>(reply.getData());
	responseHeaderPrefix.fromBigEndian();
	if (responseHeaderPrefix.status == ProtocolStatus::INVALID_VERSION) {
		// The server doesn't understand this version of the header
		// protocol. Since this library only runs version 1 of the
		// protocol, this shouldn't happen if servers continue supporting
		// version 1.
		PANIC(
				"This client is too old to talk to the server. " "You'll need to update your client library.");
	}

	if (reply.getLength() < sizeof(ResponseHeaderVersion1)) {
		PANIC(
				"The response from the server was too short to be valid. " "This probably indicates network or memory corruption.");
	}
	ResponseHeaderVersion1 responseHeader =
			*static_cast<const ResponseHeaderVersion1*>(reply.getData());
	responseHeader.fromBigEndian();

	switch (responseHeader.prefix.status) {

		// The RPC succeeded. Parse the response into a protocol buffer.
		case ProtocolStatus::OK:
			if (response != NULL
					&& !RPC::ProtoBuf::parse(reply, *response,
							sizeof(responseHeader))) {
				PANIC(
						"Could not parse the protocol buffer out of the server " "response");
			}
			return Status::OK;

			// The RPC failed in a service-specific way. Parse the response into a
			// protocol buffer.
		case ProtocolStatus::SERVICE_SPECIFIC_ERROR:
			if (serviceSpecificError != NULL
					&& !RPC::ProtoBuf::parse(reply, *serviceSpecificError,
							sizeof(responseHeader))) {
				PANIC(
						"Could not parse the protocol buffer out of the " "service-specific error details");
			}
			return Status::SERVICE_SPECIFIC_ERROR;

			// The server does not have the requested service.
		case ProtocolStatus::INVALID_SERVICE:
			PANIC("The server is not running the requested service.");

			// The server disliked our request. This shouldn't happen because
			// the higher layers of software were supposed to negotiate an RPC
			// protocol version.
		case ProtocolStatus::INVALID_REQUEST:
			PANIC(
					"The server found the request to be invalid. This " "indicates a bug in the client or server in negotiating " "which RPCs the client may legally send to the server.");

		default:
			// The server shouldn't reply back with status codes we don't
			// understand. That's why we gave it a version number in the
			// request header.
			PANIC(
					"Unknown status %u returned from server after sending it " "protocol version 1 in the request header. This probably " "indicates a bug in the server.",
					uint32_t(responseHeader.prefix.status));
	}

}*/

void ZeromqClientRPC::update() {
	if (!ready && session)
		session->update(*this);
}

}  // namespace LogCabin::RPC
}  // namespace LogCabin
