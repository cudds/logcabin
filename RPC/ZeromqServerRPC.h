#include "RPC/ZeromqOpaqueServerRPC.h"
#include "RPC/Protocol.h"
#include <google/protobuf/message.h>

#ifndef LOGCABIN_ZEROMQ_SERVERRPC_H
#define LOGCABIN_ZEROMQ_SERVERRPC_H

namespace LogCabin {
namespace RPC {

/*
 ZeromqServerRPC deals with the header of data field of Buffer, which is service specific
 ZeromqServerRPC uses ZeromqOpaqueServerRPC to initialize itself
 */
class ZeromqServerRPC {
	//explicit requires that ZeromqServerRPC needs to be constructed and passed to the function instead of compiler implicitly converting to ZeromqServerRPC
	explicit ZeromqServerRPC(ZeromqOpaqueServerRPC opaqueRPC,
			std::shared_ptr<zmq::context_t> context);

public:
	ZeromqServerRPC();
	//move constructor
	ZeromqServerRPC(ZeromqServerRPC&& other);
	//deconstructor
	~ZeromqServerRPC();
	/*
	 Move assignment.
	 */
	ZeromqServerRPC& operator=(ZeromqServerRPC&& other);

	uint16_t getService() const {
		return service;
	}
	uint16_t getOpCode() const {
		return opCode;
	}

	void rejectInvalidRequest();
	void rejectInvalidRequest(ZeromqPub& pub);

	bool getRequest(google::protobuf::Message& request);

	void returnError(const google::protobuf::Message& serviceSpecificError, ZeromqPub& pub);

	void reply(const google::protobuf::Message& payload, ZeromqPub& pub);

	void closeSession();
	std::shared_ptr<zmq::context_t> context;

private:
	ZeromqOpaqueServerRPC opaqueRPC;
	/// See getService().
	uint16_t service;

	uint8_t serviceSpecificErrorVersion;
	/// See getOpCode().
	uint16_t opCode;

	bool active;

	void reject(RPC::Protocol::Status status, ZeromqPub& pub);

	// ZeromqServerRPC is non-copyable.
	ZeromqServerRPC(const ZeromqServerRPC&) = delete;
	ZeromqServerRPC& operator=(const ZeromqServerRPC&) = delete;

	friend class ZeromqServer;
};

}  //end of RPC
}  //end of LogCabin

#endif
