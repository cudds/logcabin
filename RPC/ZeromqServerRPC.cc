#include "RPC/ZeromqServerRPC.h"
#include "RPC/ProtoBuf.h"
#include "Core/Debug.h"

namespace LogCabin {
namespace RPC {

using RPC::Protocol::RequestHeaderPrefix;
using RPC::Protocol::RequestHeaderVersion1;
using RPC::Protocol::ResponseHeaderPrefix;
using RPC::Protocol::ResponseHeaderVersion1;
using RPC::Protocol::Status;

ZeromqServerRPC::ZeromqServerRPC(ZeromqOpaqueServerRPC opaqueRPC,
		std::shared_ptr<zmq::context_t> context)
		: opaqueRPC(std::move(opaqueRPC)), service(0), serviceSpecificErrorVersion(
				0), opCode(0), active(true), context(context) {
	const Buffer& request = this->opaqueRPC.request;

	// Carefully read the headers.
	if (request.getLength() < sizeof(RequestHeaderPrefix)) {
		ZeromqPub pub(context);
		std::string error = pub.connect(opaqueRPC.smID);
		assert(error == "");
		reject(Status::INVALID_REQUEST, pub);
		return;
	}
	RequestHeaderPrefix requestHeaderPrefix =
			*static_cast<const RequestHeaderPrefix*>(request.getData());
	requestHeaderPrefix.fromBigEndian();
	if (requestHeaderPrefix.version != 1
			|| request.getLength() < sizeof(RequestHeaderVersion1)) {
		ZeromqPub pub(context);
		std::string error = pub.connect(opaqueRPC.smID);
		assert(error == "");
		reject(Status::INVALID_VERSION, pub);
		return;
	}
	RequestHeaderVersion1 requestHeader =
			*static_cast<const RequestHeaderVersion1*>(request.getData());
	requestHeader.fromBigEndian();

	service = requestHeader.service;
	serviceSpecificErrorVersion = requestHeader.serviceSpecificErrorVersion;
	opCode = requestHeader.opCode;
}

ZeromqServerRPC::ZeromqServerRPC()
		: opaqueRPC(), service(0), serviceSpecificErrorVersion(0), opCode(0), active(
				false), context(NULL) {
}

ZeromqServerRPC::ZeromqServerRPC(ZeromqServerRPC&& other)
		: opaqueRPC(std::move(other.opaqueRPC)), service(other.service), serviceSpecificErrorVersion(
				other.serviceSpecificErrorVersion), opCode(other.opCode), active(
				other.active), context(other.context) {
}

ZeromqServerRPC&
ZeromqServerRPC::operator=(ZeromqServerRPC&& other) {
	opaqueRPC = std::move(other.opaqueRPC);
	active = other.active;
	service = other.service;
	serviceSpecificErrorVersion = other.serviceSpecificErrorVersion;
	opCode = other.opCode;
	context = other.context;
	return *this;
}

ZeromqServerRPC::~ZeromqServerRPC() {
}

void ZeromqServerRPC::rejectInvalidRequest(ZeromqPub& pub) {
	reject(Status::INVALID_REQUEST, pub);
}

void ZeromqServerRPC::rejectInvalidRequest() {
	ZeromqPub pub(context);
	std::string error = pub.connect(opaqueRPC.smID);
	assert(error == "");
	reject(Status::INVALID_REQUEST, pub);
}

bool ZeromqServerRPC::getRequest(google::protobuf::Message& request) {
	if (!active)
		return false;
	if (!RPC::ProtoBuf::parse(opaqueRPC.request, request,
			sizeof(RequestHeaderVersion1))) {
		rejectInvalidRequest();
		return false;
	}
	return true;
}

void ZeromqServerRPC::returnError(
		const google::protobuf::Message& serviceSpecificError, ZeromqPub& pub) {
	active = false;
	RPC::Buffer buffer;
	RPC::ProtoBuf::serialize(serviceSpecificError, buffer,
			sizeof(ResponseHeaderVersion1));
	auto& responseHeader =
			*static_cast<ResponseHeaderVersion1*>(buffer.getData());
	responseHeader.prefix.status = Status::SERVICE_SPECIFIC_ERROR;
	responseHeader.prefix.toBigEndian();
	responseHeader.toBigEndian();
	opaqueRPC.response = std::move(buffer);
	opaqueRPC.sendReply(pub);
}

void ZeromqServerRPC::reject(RPC::Protocol::Status status, ZeromqPub& pub) {
	active = false;
	ResponseHeaderVersion1& responseHeader = *new ResponseHeaderVersion1();
	responseHeader.prefix.status = status;
	responseHeader.prefix.toBigEndian();
	responseHeader.toBigEndian();
	opaqueRPC.response.setData(&responseHeader, sizeof(responseHeader),
			RPC::Buffer::deleteObjectFn<ResponseHeaderVersion1*>);
	opaqueRPC.sendReply(pub);
}

void ZeromqServerRPC::reply(const google::protobuf::Message& payload, ZeromqPub& pub) {
	active = false;
	RPC::Buffer buffer;
	RPC::ProtoBuf::serialize(payload, buffer, sizeof(ResponseHeaderVersion1));
	auto& responseHeader =
			*static_cast<ResponseHeaderVersion1*>(buffer.getData());
	responseHeader.prefix.status = Status::OK;
	responseHeader.prefix.toBigEndian();
	responseHeader.toBigEndian();
	opaqueRPC.response = std::move(buffer);
	opaqueRPC.sendReply(pub);
}

void ZeromqServerRPC::closeSession() {
	active = false;
	opaqueRPC.closeSession();
}

}
}

