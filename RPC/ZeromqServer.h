//written by ning gao in University of Colorado Boulder 
//<nigo9731@colorado.edu>

#include <mutex>
#include <unordered_map>
#include <memory>
#include "Buffer.h"
#include "RPC/ThreadDispatchService.h"
#include "RPC/ZeromqOpaqueServerRPC.h"
#include "RPC/zeromq/ZeromqReceiver.h"

#ifndef LOGCABIN_ZEROMQ_SERVER_H
#define LOGCABIN_ZEROMQ_SERVER_H

namespace LogCabin {
namespace RPC {
/*
 ZeromqServer waits for requests from clients or leaders and dispatches the request to a worker thread.
 It uses ZeromqReceiver to bind to an internal port of a router-router thread. 
 */
class ZeromqServer {
public:
	ZeromqServer(std::shared_ptr<zmq::context_t> context);
	~ZeromqServer();

	void registerService(uint16_t serviceId, std::shared_ptr<Service> service, uint32_t maxThreads, std::string smID);

	ZeromqOpaqueServerRPC waitForRPC(bool &breakRun);

	std::string connect(std::string smID);

	void handleRPC(ZeromqOpaqueServerRPC opaqueRPC);
	void close();

private:
	std::unordered_map<uint16_t, std::shared_ptr<Service>> services;

	//	only ZeromqServer has an instance of ZeromqReceiver to process requests from client or leader
	std::unique_ptr<ZeromqReceiver> receiver;

	//	shared context among services 
	std::shared_ptr<zmq::context_t> context;

	//not copyable
	ZeromqServer(const ZeromqServer&) = delete;
	ZeromqServer& operator=(const ZeromqServer&) = delete;

};

}
}
#endif
