#include <memory>
#include "RPC/Buffer.h"
#include "RPC/zeromq/ZeromqPub.h"
#ifndef LOGCABIN_RPC_ZEROMQOPAQUESERVERRPC_H
#define LOGCABIN_RPC_ZEROMQOPAQUESERVERRPC_H

namespace LogCabin {
namespace RPC {

/*	
 ZeromqOpaqueServerRPC deals with RPC calls and maintains the state information of the RPC request
 */
class ZeromqOpaqueServerRPC {

public:
	ZeromqOpaqueServerRPC();
	ZeromqOpaqueServerRPC(std::string smID_, uint64_t messageId_,
			std::basic_string<unsigned char> client_id_, Buffer request_);
	ZeromqOpaqueServerRPC(ZeromqOpaqueServerRPC&& other);
	~ZeromqOpaqueServerRPC();
	ZeromqOpaqueServerRPC& operator=(ZeromqOpaqueServerRPC&& other);
	/*
	 State information necessary to reply to the client/leader asynchronously from the ZeromqServer
	 */
	Buffer request;
	Buffer response;
	std::string smID;
	uint64_t messageId;  //-1 means it's empty. It's for breaking runforever in Loop
	std::basic_string<unsigned char> client_id;

	void sendReply(ZeromqPub& pub);
	void closeSession();

private:
	//ZeromqOpaqueServerRPC is non-copyable.
	ZeromqOpaqueServerRPC(const ZeromqOpaqueServerRPC&) = delete;
	ZeromqOpaqueServerRPC& operator=(const ZeromqOpaqueServerRPC&) = delete;

};
}
}

#endif
