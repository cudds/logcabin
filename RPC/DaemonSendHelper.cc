//written by ning gao in University of Colorado Boulder 
//<nigo9731@colorado.edu>
#include "logcabin/RPC/DaemonSendHelper.h"
#include <vector>
#include "Core/ProtoBuf.h"
#include "build/Protocol/Daemon.pb.h"
#include "Core/StringUtil.h"
namespace LogCabin {
namespace RPC {

DaemonSendHelper::DaemonSendHelper(std::shared_ptr<zmq::context_t> context)
		: context(context) {

}

DaemonSendHelper::~DaemonSendHelper() {

}

bool DaemonSendHelper::send(Kind kind, std::string servers, std::string smID,
		uint16_t serverID, std::string port) {
	RPC::ZeromqDaemonSend sender(context);
	std::vector<std::string> addresses = Core::StringUtil::split(servers, ';');
	if (addresses.size() < serverID || serverID <= 0)
		return false;
	sender.connect(addresses[serverID - 1], port);

	Protocol::Daemon::Request request;
	if (kind == Kind::CREATE)
		request.set_kind(Protocol::Daemon::Kind::CREATE);
	else
		request.set_kind(Protocol::Daemon::Kind::DELETE);
	request.set_smid(smID);
	request.set_serverid(serverID);
	for (int i = 0; i < addresses.size(); i++) {
		request.add_servers(addresses[i]);
	}

	uint32_t length = request.ByteSize();
	char* data = new char[length];
	request.SerializeToArray(data, length);
	sender.sendMessage((unsigned char *) data, length);
	delete[] data;
}

}  //end of Server
}  //end of LogCabin
