#include "Buffer.h"

namespace LogCabin {
namespace RPC {

Buffer::Buffer()
		: data(NULL), length(0), deleter(NULL) {
}

Buffer::Buffer(void* data, uint32_t length, Deleter deleter)
		: data(data), length(length), deleter(deleter) {
}

Buffer::Buffer(Buffer&& other)
		: data(other.data), length(other.length), deleter(other.deleter) {
	other.data = NULL;  // Not strictly necessary, but may help
	other.length = 0;  // catch bugs in caller code.
	other.deleter = NULL;
}

Buffer::~Buffer() {
	if (deleter != NULL)
		(*deleter)(data);
}

Buffer&
Buffer::operator=(Buffer&& other) {
	if (deleter != NULL)
		(*deleter)(data);
	data = other.data;
	length = other.length;
	deleter = other.deleter;
	other.data = NULL;  // Not strictly necessary, but may help
	other.length = 0;  // catch bugs in caller code.
	other.deleter = NULL;
	return *this;
}

void Buffer::setData(void* data, uint32_t length, Deleter deleter) {
	if (this->deleter != NULL)
		(*this->deleter)(this->data);
	this->data = data;
	this->length = length;
	this->deleter = deleter;
}

void Buffer::reset() {
	if (deleter != NULL)
		(*deleter)(data);
	data = NULL;
	length = 0;
	deleter = NULL;
}

}  // namespace LogCabin::RPC
}  // namespace LogCabin
