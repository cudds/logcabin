//written by ning gao in University of Colorado Boulder 
//<nigo9731@colorado.edu>

#include "zmsg.hpp"
#include <queue>
#include <mutex>
#include <memory>

#ifndef LOGCABIN_RPC_ZEROMQDAEMONRECV_H
#define LOGCABIN_RPC_ZEROMQDAEMONRECV_H

namespace LogCabin {
namespace RPC {
/*
 ZeromqDaemonRecv is a wrapper class around zeromq to open a socket to wait for Daemon requests
 It's only used by Daemon thread
 One needs to call sendReady after wait to make it either blocking or nonblocking.
 */

class ZeromqDaemonRecv {
public:
	ZeromqDaemonRecv(std::shared_ptr<zmq::context_t> context);
	~ZeromqDaemonRecv();

	std::string bind(std::string port);
	/*
	 wait is called to waiting for service requests
	 breakRun can be assigned to break the blocking call.
	 */
	void *wait(int &size, bool &breakRun);
	/*
	 * sendReady send ok to the requester.
	 * it's used for blocking RPC calls.
	 */
	void sendReady();
	/*
	 context and socket are closed
	 */
	void close();

private:
	zmq::socket_t socket;
	//ZeromqDaemonRecv non-copyable
	ZeromqDaemonRecv(const ZeromqDaemonRecv&) = delete;
	ZeromqDaemonRecv& operator=(const ZeromqDaemonRecv&) = delete;

};

}
}
#endif
