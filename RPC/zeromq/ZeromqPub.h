//written by ning gao in University of Colorado Boulder 
//<nigo9731@colorado.edu>

#include "zmsg.hpp"
#include <string>
#include <memory>

#ifndef LOGCABIN_RPC_ZEROMQPUB_H
#define LOGCABIN_RPC_ZEROMQPUB_H

namespace LogCabin {
namespace RPC {

class ZeromqPub {
public:
	/*
	 Constructor 
	 Context is shared so that inproc works
	 ZeromqPub is used by ZeromqOpaqueServerRPC sendReply
	 */
	ZeromqPub(std::shared_ptr<zmq::context_t> context);
	~ZeromqPub();
	std::string connect(std::string smID_);
	/*
	 sendMessage sends a response back for the previous request.
	 */
	void sendMessage(std::basic_string<unsigned char> client_id,
			uint64_t messageId, void *data, uint32_t length);
private:
	zmq::socket_t socket;
	std::string smID;
};

}  //end of RPC
}  //end of LogCabin

#endif
