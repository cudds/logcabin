//written by ning gao in University of Colorado Boulder 
//<nigo9731@colorado.edu>

#include "zmsg.hpp"
#include <queue>
#include <mutex>
#include <memory>

#ifndef LOGCABIN_RPC_ZEROMQRECEIVER_H
#define LOGCABIN_RPC_ZEROMQRECEIVER_H

namespace LogCabin {
namespace RPC {

/*	
 start_router starts a router thread, the front_address accepting requests from clients and routes their requests
 to corresponding state machines, which is connected to the back_address.
 params
 front_address: tcp://*:port1
 back_address: tcp://*:port2	
 terminate_router: passed from the outside function, to terminate the thread.
 No locks are needed when modifying terminate_router.
 */
void start_router(std::string front_address, std::string back_address,
		std::shared_ptr<bool> terminate_router);

/*
 start_proxy starts a proxy connecting the router with Event Loop/ZeromqServers
 It's designed to avoid locks in ZeromqReceiver, which is shared between different threads to reply rpc request.
 params
 address: tcp://localhost:5555
 smID: sm1
 terminate_proxy: passed from the outside function, to terminate the thread
 context: used to shared among service requests to support inproc communication
 */
/*
 * start_proxy is used in unit test
 * startProxy supports two stage exits for safe exit during sigint
 */
void startProxy(std::string address, std::string smID,
		std::shared_ptr<bool> terminate_proxy, std::shared_ptr<bool> shutIncome_proxy,
		std::shared_ptr<zmq::context_t> context);

void start_proxy(std::string address, std::string smID,
		std::shared_ptr<bool> terminate_proxy,
		std::shared_ptr<zmq::context_t> context);

/*
 ZeromqReceiver is a utility class to maintain a connection to back_address opened by start_router.
 It's used by EventLoop to wait for requests(client service/raft service).
 TODO:	remove locks and use in-proc socket to achieve asynchronous requests and responds
 */
class ZeromqReceiver {
public:
	ZeromqReceiver(std::shared_ptr<zmq::context_t> context);
	~ZeromqReceiver();
	/*
	 connect to the pub address, which is opened by the start_router
	 register its state machine ID.
	 params
	 smID_: sm1(or any string agreed)
	 */
	std::string connect(std::string smID_);
	/*
	 wait is called to waiting for service requests
	 breakRun can be assigned to break the blocking call.
	 */
	void *wait(std::string &smID_, int &size, uint64_t& messageId,
			std::basic_string<unsigned char> &client_id, bool &breakRun);
	/*
	 context and socket are closed
	 */
	void close();

private:
	zmq::socket_t socket;
	std::string smID;
	/*
	 provide locking for bind, wait and sendMessage
	 the shared data is socket
	 */
	mutable std::mutex mutex;

	//ZeromqReceiver non-copyable
	ZeromqReceiver(const ZeromqReceiver&) = delete;
	ZeromqReceiver& operator=(const ZeromqReceiver&) = delete;

};

}
}
#endif
