#include "ZeromqReceiver.h"
#include "Core/Debug.h"
#include "Core/ThreadId.h"
#include <time.h>
#include "Core/Debug.h"
namespace LogCabin {
namespace RPC {
ZeromqReceiver::ZeromqReceiver(std::shared_ptr<zmq::context_t> context)
		: socket(*context, ZMQ_REP) {
	int lingtime = 10;
	socket.setsockopt(ZMQ_LINGER, &lingtime, sizeof(int));
}
ZeromqReceiver::~ZeromqReceiver() {
	close();
}

std::string ZeromqReceiver::connect(std::string smID_) {
	smID = smID_;
	try {
		socket.connect((std::string("inproc://") + smID + std::string("_req")).c_str());
	} catch (const error_t& error) {
		return "ZeromqReceiver cannot bind(connect) to the address";
	}
	return "";
}

void ZeromqReceiver::close() {
	socket.close();
}

/*
 frames to get
 REQ IDENTITY wraps with id+empty but DEALER adds only id
 client_id+smID+content
 usleep can be removed when in-proc routing is done
 */
void *ZeromqReceiver::wait(std::string &smID_, int &size, uint64_t& messageId,
		std::basic_string<unsigned char> &client_id, bool &breakRun) {
	zmq::pollitem_t items[] = { { socket, 0, ZMQ_POLLIN, 0 }, };

	zmsg zm;

	while (true) {
		zmq::poll(items, 1, 50);
		if (items[0].revents & ZMQ_POLLIN) {
			break;
		}
		if (breakRun)
			return NULL;
	}

	zm.recv(socket);
	client_id = zm.get_ustring(0);
	//get the real content
	std::basic_string<unsigned char> request = zm.get_part(2);
	assert(!request.empty());
	size = request.size() - 8;
	const unsigned char *startId = request.data();

	for (int i = size + 7; i >= size; i--) {
		uint8_t num = *(((uint8_t*) startId) + i);
		messageId = (messageId << 8) + num;
	}
	uint8_t *requestArray = new uint8_t[size];
	memcpy(requestArray, request.data(), size);
	s_send(socket, "OK");
	smID_ = smID;
	return requestArray;
}

/*
 frames recved from the backend
 smid+empty+client_id+empty+smID+content
 frames sent to the frontend
 client_id+empty+smID+content

 frames recved from the frontend
 client_id+smID+content
 frames sent to the backend
 smid+empty+client_id+smID+content
 */

void start_router(std::string frontPort, std::string backPort,
		std::shared_ptr<bool> terminate_router) {
	Core::ThreadId::setName("SMRouter");
	zmq::context_t context(1);
	zmq::socket_t frontend(context, ZMQ_ROUTER);
	zmq::socket_t backend(context, ZMQ_ROUTER);
	std::string localAddress = "tcp://*";
	std::string frontAddress = localAddress + ":" + frontPort;
	std::string backAddress = localAddress + ":" + backPort;
	frontend.bind(frontAddress.c_str());
	backend.bind(backAddress.c_str());

	int lingtime = 10;
	frontend.setsockopt(ZMQ_LINGER, &lingtime, sizeof(int));
	backend.setsockopt(ZMQ_LINGER, &lingtime, sizeof(int));

	while (true) {
		if (*terminate_router)
			return;

		zmq::pollitem_t items[] = { { backend, 0, ZMQ_POLLIN, 0 }, { frontend, 0, ZMQ_POLLIN, 0 } };
		zmq::poll(items, 2, 50);  //the third argument is to set timeout for blocking call to poll

		//  Handle worker activity on backend
		if (items[0].revents & ZMQ_POLLIN) {
			zmsg zm(backend);
			zm.unwrap();
			if (strcmp(zm.address(), "HANDSHAKE") == 0) {
				NOTICE("handshake received");
				zm.clear();
			} else
				zm.send(frontend);
		}
		if (items[1].revents & ZMQ_POLLIN) {
			zmsg zm(frontend);
			std::basic_string<unsigned char> smID = zm.get_part(1);
			zm.wrap((const char *) smID.c_str(), "");
			zm.send(backend);
		}
	}
}

void startProxy(std::string backPort, std::string smID, std::shared_ptr<bool> terminate_proxy, std::shared_ptr<bool> shutIncome_proxy,
		std::shared_ptr<zmq::context_t> context) {
	Core::ThreadId::setName(smID+":proxy");
	zmq::socket_t frontend(*context, ZMQ_DEALER);
	zmq::socket_t backend_Server(*context, ZMQ_DEALER);
	zmq::socket_t backend_Service(*context, ZMQ_REP);

	std::string localAddress = "tcp://localhost";
	std::string backAddress = localAddress + ":" + backPort;
	frontend.setsockopt(ZMQ_IDENTITY, smID.c_str(), smID.length());
	frontend.connect(backAddress.c_str());

	//send handshake
	if (!s_send(frontend, "HANDSHAKE")) {
		std::cout << "start_proxy cannot bind(send) to the address handshake" << std::endl;
		return;
	}
	backend_Server.bind((std::string("inproc://") + smID + std::string("_req")).c_str());
	backend_Service.bind((std::string("inproc://") + smID + std::string("_rep")).c_str());

	int lingtime = 10;
	frontend.setsockopt(ZMQ_LINGER, &lingtime, sizeof(int));
	backend_Server.setsockopt(ZMQ_LINGER, &lingtime, sizeof(int));
	backend_Service.setsockopt(ZMQ_LINGER, &lingtime, sizeof(int));

	while (true) {
		if (*terminate_proxy){
			frontend.close();
			backend_Service.close();
			backend_Server.close();
			return;
		}
		zmq::pollitem_t items[] = { { backend_Service, 0, ZMQ_POLLIN, 0 }, { frontend, 0,
				ZMQ_POLLIN, 0 } };
		zmq::poll(items, 2, 50);

		//  backend server
		//  no need to listen because it's always publishing
		//  backend service
		if (items[0].revents & ZMQ_POLLIN) {
			zmsg zm(backend_Service);
			zm.send(frontend);
			s_send(backend_Service, "OK");
		}
		//  frontend
		if (items[1].revents & ZMQ_POLLIN) {
			if(*shutIncome_proxy){
				continue;
			}
			zmsg zm(frontend);
			zm.send(backend_Server);
			zm = zmsg(backend_Server);
		}
	}
}

void start_proxy(std::string backPort, std::string smID, std::shared_ptr<bool> terminate_proxy,
		std::shared_ptr<zmq::context_t> context) {
	Core::ThreadId::setName(smID+":proxy");
	zmq::socket_t frontend(*context, ZMQ_DEALER);
	zmq::socket_t backend_Server(*context, ZMQ_DEALER);
	zmq::socket_t backend_Service(*context, ZMQ_REP);

	std::string localAddress = "tcp://localhost";
	std::string backAddress = localAddress + ":" + backPort;
	frontend.setsockopt(ZMQ_IDENTITY, smID.c_str(), smID.length());
	frontend.connect(backAddress.c_str());

	//send handshake
	if (!s_send(frontend, "HANDSHAKE")) {
		std::cout << "start_proxy cannot bind(send) to the address handshake" << std::endl;
		return;
	}
	backend_Server.bind((std::string("inproc://") + smID + std::string("_req")).c_str());
	backend_Service.bind((std::string("inproc://") + smID + std::string("_rep")).c_str());

	int lingtime = 10;
	frontend.setsockopt(ZMQ_LINGER, &lingtime, sizeof(int));
	backend_Server.setsockopt(ZMQ_LINGER, &lingtime, sizeof(int));
	backend_Service.setsockopt(ZMQ_LINGER, &lingtime, sizeof(int));

	while (true) {
		if (*terminate_proxy){
			frontend.close();
			backend_Service.close();
			backend_Server.close();
			return;
		}
		zmq::pollitem_t items[] = { { backend_Service, 0, ZMQ_POLLIN, 0 }, { frontend, 0,
				ZMQ_POLLIN, 0 } };
		zmq::poll(items, 2, 50);

		//  backend server
		//  no need to listen because it's always publishing
		//  backend service
		if (items[0].revents & ZMQ_POLLIN) {
			zmsg zm(backend_Service);
			zm.send(frontend);
			s_send(backend_Service, "OK");
		}
		//  frontend
		if (items[1].revents & ZMQ_POLLIN) {
			zmsg zm(frontend);
			zm.send(backend_Server);
			zm = zmsg(backend_Server);
		}
	}
}

}       	//end of RPC
}       	//end of LogCabin
