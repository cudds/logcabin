//written by ning gao in University of Colorado Boulder 
//<nigo9731@colorado.edu>
#include "logcabin/RPC/zeromq/ZeromqDaemonSend.h"
//#include "Core/Debug.h"

namespace LogCabin {
namespace RPC {

ZeromqDaemonSend::ZeromqDaemonSend(std::shared_ptr<zmq::context_t> context)
		: socket(*context, ZMQ_REQ) {
	int lingtime = 10;
	socket.setsockopt(ZMQ_LINGER, &lingtime, sizeof(int));
}

ZeromqDaemonSend::~ZeromqDaemonSend() {
	socket.close();
}

std::string ZeromqDaemonSend::connect(std::string address, std::string port) {
	try {
		socket.connect(
				(std::string("tcp://") + address + std::string(":") + port).c_str());
	} catch (const error_t& error) {
		return "ZeromqDaemonSend cannot bind(connect) to the address";
	}
	return "";
}

void ZeromqDaemonSend::sendMessage(unsigned char *data, uint32_t length) {
	zmsg zm;
	zm.push_back(data, length);
	zm.send(socket);
	zm = zmsg(socket);
}

}  //end of RPC
}  //end of LogCabin
