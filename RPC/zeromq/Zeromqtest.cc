//written by ning gao in University of Colorado Boulder 
//<nigo9731@colorado.edu>
#include <iostream>
#include <thread>
#include <vector>
#include "ZeromqSender.h"
#include "ZeromqReceiver.h"
#include "ZeromqPub.h"
#include "ZeromqDaemonRecv.h"
#include "ZeromqDaemonSend.h"

void send_to(std::string content, std::string smID, int messageID) {
	std::string address = "tcp://localhost:61023";
	LogCabin::RPC::ZeromqSender *sender = new LogCabin::RPC::ZeromqSender(smID);
	sender->connect(address);

	//performance evaluation
	int num = 0;
	while (num < 100000) {
		num++;
		sender->sendMessage(messageID, (void *) content.c_str(),
				content.size());
		int size = 0;
		uint64_t messageId;
		char* response = (char *) sender->wait(size, messageId);
		/*
		 std::cout<<"sender: messageId="<<messageId<<std::endl;
		 for(int i=0;i<size;i++)
		 {
		 std::cout<<*response;
		 response++;
		 }
		 std::cout<<std::endl;
		 */
		delete response;
	}
	delete sender;
}
void sender_main() {
	send_to("hello1", "sm1", 123);
	//send_to("hello2","sm2",234);
}
std::shared_ptr<LogCabin::RPC::ZeromqReceiver> receiver1;
//std::shared_ptr<LogCabin::RPC::ZeromqReceiver> receiver2;
void receiver1_main(std::shared_ptr<zmq::context_t> context) {
	int size;
	uint64_t messageId;
	std::basic_string<unsigned char> client_id;
	bool breakRun = false;
	std::string smID;
	int num = 0;
	while (num < 100000) {
		num++;
		char *request = (char *) receiver1->wait(smID, size, messageId,
				client_id, breakRun);
		if (num % 100 == 0)
			std::cout << "receiving " << num << " of requests" << std::endl;
		/*
		 std::cout<<"receiver1: messageId="<<messageId<<std::endl;
		 for(int i=0;i<size;i++)
		 {
		 std::cout<<*request;
		 request++;
		 }
		 std::cout<<std::endl;
		 */
		LogCabin::RPC::ZeromqPub zeromqPub(context);
		zeromqPub.connect(std::string("sm1"));
		zeromqPub.sendMessage(client_id, messageId, (void *) "world1", 6);

	}
}
/*
 void receiver2_main(std::shared_ptr<zmq::context_t> context)
 {
 int size;
 uint64_t messageId;
 std::basic_string<unsigned char> client_id;
 bool breakRun=false;
 std::string smID;
 char *request=(char *)receiver2->wait(smID,size,messageId,client_id,breakRun);
 std::cout<<"receiver2: messageId"<<messageId<<std::endl;
 for(int i=0;i<size;i++)
 {
 std::cout<<*request;
 request++;
 }
 std::cout<<std::endl;
 LogCabin::RPC::ZeromqPub zeromqPub(context);
 zeromqPub.connect(std::string("sm2"));
 zeromqPub.sendMessage(client_id, messageId,(void *)"world2",6);
 }
 */
/*
 test state machine related zeromq interface
 int main(void)
 {
 std::shared_ptr<zmq::context_t> context=std::make_shared<zmq::context_t>(1);
 std::shared_ptr<bool> terminate_router=std::make_shared<bool>(false);
 std::shared_ptr<bool> terminate_proxy1=std::make_shared<bool>(false);
 //std::shared_ptr<bool> terminate_proxy2=std::make_shared<bool>(false);
 std::thread router(LogCabin::RPC::start_router,std::string("tcp://*:61023"),std::string("tcp://*:5555"),terminate_router);//when specifying binding address, use * instead of localhost
 std::thread proxy1(LogCabin::RPC::start_proxy,std::string("tcp://localhost:5555"),std::string("sm1"),terminate_proxy1, context);
 //std::thread proxy2(LogCabin::RPC::start_proxy,std::string("tcp://localhost:5555"),std::string("sm2"),terminate_proxy2, context);

 receiver1=std::make_shared<LogCabin::RPC::ZeromqReceiver>(context);
 //receiver2=std::make_shared<LogCabin::RPC::ZeromqReceiver>(context);
 receiver1->connect(std::string("sm1"));	
 //receiver2->connect(std::string("sm2"));	
 std::thread receiver1_1(receiver1_main,context);
 //std::thread receiver2_1(receiver2_main,context);
 sleep(1);
 //wait for handshake to happen before sending messages
 //wait for connection to establish before messages are published

 std::thread sender(sender_main);

 if(receiver1_1.joinable())
 receiver1_1.join();
 std::cout<<"receiver1 joined"<<std::endl;

 *terminate_proxy1=true;
 proxy1.join();
 std::cout<<"proxy1 joined"<<std::endl;
 //*terminate_proxy2=true;
 //proxy2.join();
 //std::cout<<"proxy2 joined"<<std::endl;
 *terminate_router=true;
 router.join();
 std::cout<<"router joined"<<std::endl;
 if(sender.joinable())
 sender.join();
 std::cout<<"sender joined"<<std::endl;
 receiver1.reset();
 //receiver2.reset();
 return 0;
 }
 */

//test daemon related zeromq interface
void receiver_thread(std::shared_ptr<LogCabin::RPC::ZeromqDaemonRecv> recvZMQ,
		std::shared_ptr<bool> shouldExit) {
	recvZMQ->bind("61022");
	int size;
	while (true) {
		char *message = (char *) recvZMQ->wait(size, *shouldExit);
		char *orig = message;
		if (message == NULL)
			break;
		for (int i = 0; i < size; i++) {
			std::cout << *message;
			message++;
		}
		std::cout << std::endl;
		delete[] orig;
	}
}
int main(void) {
	std::shared_ptr<bool> shouldExit = std::make_shared<bool>(false);
	std::shared_ptr<zmq::context_t> context = std::make_shared<zmq::context_t>(
			1);
	std::shared_ptr<LogCabin::RPC::ZeromqDaemonRecv> recvZMQ = std::make_shared<
			LogCabin::RPC::ZeromqDaemonRecv>(context);
	std::thread receiverThread(receiver_thread, recvZMQ, shouldExit);

	LogCabin::RPC::ZeromqDaemonSend sender = LogCabin::RPC::ZeromqDaemonSend(
			context);
	sender.connect("128.138.244.47", "61022");
	sender.sendMessage((unsigned char *) "hello", 5);
	sender.sendMessage((unsigned char *) "world", 5);

	*shouldExit = true;
	receiverThread.join();
	return 0;

}
