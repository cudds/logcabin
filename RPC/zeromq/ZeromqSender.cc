//written by ning gao in University of Colorado Boulder 
//<nigo9731@colorado.edu>

#include "ZeromqSender.h"
namespace LogCabin {
namespace RPC {

ZeromqSender::ZeromqSender(std::string smID_, int type)
		: context(1), socket(context, type), smID(smID_) {
}

bool ZeromqSender::connect(std::string address) {
	try {
		socket.connect(address.c_str());
	} catch (zmq::error_t e) {
		std::cout << "error connecting to " << address << std::endl;
		return false;
	}
	return true;
}

//send sm_id+data frame
void ZeromqSender::sendMessage(uint64_t messageId, void *data,
		uint32_t length) {
	zmsg zm;
	zm.push_back(smID.c_str());
	unsigned char *data_ = new unsigned char[length + 8];
	for (int i = 0; i < length; i++)
		data_[i] = ((unsigned char*) data)[i];
	for (int i = 0; i < 8; i++)
		data_[i + length] = *((unsigned char *) (&messageId) + i);

	//append messageId in the data to distinguish messages from asynchronous calls to sendMessage
	//it may need to deal with situations where data contains null characters
	zm.push_back(data_, length + 8);
	//zm.dump();
	zm.send(socket);
	delete[] data_;
}

//the returned array needs to be deallocated by outside functions
void *ZeromqSender::wait(int &size, uint64_t &messageId) {
	zmsg zm(socket);

	std::basic_string<unsigned char> response = zm.get_part(1);
	assert(!response.empty());
	size = response.size() - 8;
	const unsigned char *startId = response.data();
	for (int i = size + 7; i >= size; i--) {
		uint8_t num = *(((uint8_t*) startId) + i);
		messageId = (messageId << 8) + num;
	}

	uint8_t *responseArray = new uint8_t[size];
	memcpy((void*) responseArray, response.data(), size);
	return responseArray;
}

//non block wait so that RPC cancel can cancel it
void *ZeromqSender::nonblock_wait(int &size, uint64_t &messageId) {
	zmsg zm;
	if (!zm.recv_non_block(socket))
		return NULL;

	std::basic_string<unsigned char> response = zm.get_part(1);
	assert(!response.empty());
	size = response.size() - 8;
	const unsigned char *startId = response.data();
	for (int i = size + 7; i >= size; i--) {
		uint8_t num = *(((uint8_t*) startId) + i);
		messageId = (messageId << 8) + num;
	}

	uint8_t *responseArray = new uint8_t[size];
	memcpy((void*) responseArray, response.data(), size);
	return responseArray;
}

}
}
