//written by ning gao in University of Colorado Boulder 
//<nigo9731@colorado.edu>

#include <string>
#include "zmsg.hpp"

#ifndef LOGCABIN_RPC_ZEROMQSENDER_H
#define LOGCABIN_RPC_ZEROMQSENDER_H

namespace LogCabin {
namespace RPC {

/*
 ZeromqSender is a utility class to maintain a connection to the remote server
 it's used by ClientSession to send service requests.
 */
class ZeromqSender {
public:
	/*
	 constructor: create context and socket
	 ZMQ_DEALER is used by default
	 DEALER is used instead of REQ because RPC calls might need to be canceled and two recv call will be called continuously
	 */
	ZeromqSender(std::string smID_, int type = ZMQ_DEALER);

	~ZeromqSender(){
		socket.close();
	}
	/*
	 make a connection to the address
	 params
	 tcp://192.168.1.2:port1
	 */
	bool connect(std::string address);
	/*
	 send data of length to the destined address
	 it will first copy the data and then send the local copy
	 because zmsg push_back doesn't do copying
	 */
	void sendMessage(uint64_t messageId, void *data, uint32_t length);
	/*
	 block wait for responses
	 it allocates the memory and returns the pointer
	 */
	void *wait(int &size, uint64_t &messageId);
	/*
	 non-block wait 
	 it's used in ClientSession wait to asynchronously poll messages so that RPC can be canceled.
	 */
	void *nonblock_wait(int &size, uint64_t &messageId);
private:
	zmq::context_t context;
	zmq::socket_t socket;
	std::string smID;
};

}  //end of RPC
}  //end of LogCabin

#endif
