#include "ZeromqPub.h"
//#include "Core/Debug.h"

namespace LogCabin {
namespace RPC {

ZeromqPub::ZeromqPub(std::shared_ptr<zmq::context_t> context)
		: socket(*context, ZMQ_REQ) {
	int lingtime = 10;
	socket.setsockopt(ZMQ_LINGER, &lingtime, sizeof(int));
}

ZeromqPub::~ZeromqPub() {
	socket.close();
}

std::string ZeromqPub::connect(std::string smID_) {
	smID = smID_;
	try {
		socket.connect((std::string("inproc://") + smID + std::string("_rep")).c_str());
	} catch (const error_t& error) {
		return "ZeromqPub cannot bind(connect) to the address";
	}
	return "";
}
/*
 four frames to send
 empty+client_id+smID+content
 this send message will send very fast because it's a guaranteed call and asynchronous sending happens behind the scenes
 */
void ZeromqPub::sendMessage(std::basic_string<unsigned char> client_id, uint64_t messageId,
		void *data, uint32_t length) {
	zmsg zm;
	zm.push_back("");
	zm.push_ustring(client_id);
	zm.push_back(smID.c_str());
	unsigned char *data_ = new unsigned char[length + 8];
	for (int i = 0; i < length; i++)
		data_[i] = ((unsigned char*) data)[i];
	for (int i = 0; i < 8; i++)
		data_[i + length] = *((unsigned char *) (&messageId) + i);
	zm.push_back(data_, length + 8);
	zm.send(socket);
	delete[] data_;
	zm = zmsg(socket);
}

}  //end of RPC
}  //end of LogCabin
