#include "czmq.h"
#include <string>
using namespace std;
#ifdef __CPLUSPLUS
extern "C" {
#endif

int router_main_c(void);
void worker_ready(const char *id, zctx_t** ctx, void** worker);
char *wait_from_router(void* worker, byte *client_id, int* client_idsize);
void send_message(void* worker, const char *message, const byte *client_id,
		int client_idsize, const char *smName);
void destroy_states(zctx_t **ctx, void **worker);

#ifdef __CPLUSPLUS
}
#endif

class zeromq_rpc {
	/*
	 * This class is to manage messages from and to a client. Now whenever there's a message from a client, it will receive and respond
	 * but it doesn't send message initiatively so that wait needs to be called before respond
	 *
	 */
public:
	zeromq_rpc();
	//a static function to start router-router thread
	static void startZeroMQ();
	//send a ready message to the router so that router will receive a request from client and forward it to the sm
	void register_sm(string smName);
	//block wait for any client request
	string wait();
	//send message to client in response to the request
	void respond(string str);
	//destroy the zeromq context and sockets
	void destroy();

public:
	zctx_t *ctx;
	void *worker;
	//the req needs to store the client_id so that it can append to the message when sending
	byte *client_id;
	int client_idsize;
	string smName;

};
