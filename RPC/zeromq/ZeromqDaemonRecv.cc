#include "ZeromqDaemonRecv.h"
#include <time.h>
namespace LogCabin {
namespace RPC {
ZeromqDaemonRecv::ZeromqDaemonRecv(std::shared_ptr<zmq::context_t> context)
		: socket(*context, ZMQ_REP) {
	int lingtime = 10;
	socket.setsockopt(ZMQ_LINGER, &lingtime, sizeof(int));
}

ZeromqDaemonRecv::~ZeromqDaemonRecv() {
	close();
}

std::string ZeromqDaemonRecv::bind(std::string port) {
	try {
		socket.bind((std::string("tcp://*:") + port).c_str());
	} catch (const error_t& error) {
		return "ZeromqDaemonRecv cannot bind(connect) to the address";
	}
	return "";
}

void ZeromqDaemonRecv::close() {
	socket.close();
}

void *ZeromqDaemonRecv::wait(int &size, bool &breakRun) {
	zmq::pollitem_t items[] = { { socket, 0, ZMQ_POLLIN, 0 }, };

	zmsg zm;
	while (true) {
		zmq::poll(items, 1, 50);
		if (items[0].revents & ZMQ_POLLIN) {
			break;
		}
		if (breakRun)
			return NULL;
	}
	zm.recv(socket);
	std::basic_string<unsigned char> request = zm.get_part(0);
	size = request.size();
	uint8_t *message = new uint8_t[size];
	memcpy(message, request.data(), size);
	return message;
}

void ZeromqDaemonRecv::sendReady(){
	s_send(socket, "OK");
}

}  //end of RPC
}  //end of LogCabin
