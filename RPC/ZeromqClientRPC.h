//written by ning gao in University of Colorado Boulder 
//<nigo9731@colorado.edu>

#include <cinttypes>
#include <google/protobuf/message.h>
#include <iostream>
#include <memory>
#include <string>
#include <mutex>

#include "RPC/Buffer.h"

#ifndef LOGCABIN_RPC_ZEROMQCLIENTRPC_H
#define LOGCABIN_RPC_ZEROMQCLIENTRPC_H

namespace LogCabin {
namespace RPC {

class ZeromqClientSession;
// forward declaration

/*
 This class represents an asynchronous remote procedure call. 
 it combines ClientRPC for a particular service and OpaqueClientRPC for dealing with session and concurrency
 */
class ZeromqClientRPC {
public:

	/*
	 Issue an RPC to a remote service.
	 params 
	 session: A connection to the remote server.
	 service: Identifies the service running on the server. See Protocol::Common::ServiceId.
	 serviceSpecificErrorVersion: This tells the Service what service-specific errors the client understands
	 opCode: Identifies the remote procedure within the Service to execute.
	 request: See Protocol::Common::ServiceId.The arguments to the remote procedure.
	 */
	ZeromqClientRPC(std::shared_ptr<RPC::ZeromqClientSession> session,
			uint16_t service, uint8_t serviceSpecificErrorVersion,
			uint16_t opCode, const google::protobuf::Message& request);
	ZeromqClientRPC();

	/*
	 Move constructor.
	 */
	ZeromqClientRPC(ZeromqClientRPC&&);

	/*
	 Destructor.
	 */
	~ZeromqClientRPC();

	/*
	 Move assignment.
	 */
	ZeromqClientRPC& operator=(ZeromqClientRPC&&);

	std::string getErrorMessage() const;

	/*
	 The return type of waitForReply().
	 */
	enum class Status {
		/**
		 * The service returned a normal response. This is available in
		 * 'response'.
		 */
		OK,
		/**
		 * The service threw an error (but at the transport level, the RPC
		 * succeeded). Service-specific details may be available in
		 * 'serviceSpecificError'.
		 */
		SERVICE_SPECIFIC_ERROR,
		/**
		 * The server could not be contacted or did not reply. It is unknown
		 * whether or not the server executed the RPC. More information is
		 * available with getErrorMessage().
		 */
		RPC_FAILED,
	};

	//Status waitForReply(google::protobuf::Message* response,
	//		google::protobuf::Message* serviceSpecificError);

	//when the remote rsm(proxy) is not running yet and the request is lost in the zeromq router in which case, the requester needs to resend again.
	Status nonBlockWaitForReply(google::protobuf::Message* response,
	                			google::protobuf::Message* serviceSpecificError);

	void cancel();
	//update this object by calling session update.
	void update();

	//used to record the message ID of the current request
	uint64_t responseToken;
	std::shared_ptr<ZeromqClientSession> session;
	Buffer reply;
	std::string errorMessage;

private:

	mutable std::mutex mutex;
	bool ready;  //the next waitForReply will respond immediately
	friend class ZeromqClientSession;
	//ZeromqClientRPC is non-copyable.
	ZeromqClientRPC(const ZeromqClientRPC&) = delete;
	ZeromqClientRPC& operator=(const ZeromqClientRPC&) = delete;
};

}  // namespace LogCabin::RPC
}  // namespace LogCabin

#endif
