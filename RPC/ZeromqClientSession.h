#include <memory>
#include <mutex>
#include <string>
#include <unordered_map>
#include "RPC/Buffer.h"
#include "RPC/Address.h"
#include "RPC/zeromq/ZeromqSender.h"

#ifndef LOGCABIN_RPC_ZEROMQCLIENTSESSION_H
#define LOGCABIN_RPC_ZEROMQCLIENTSESSION_H

namespace LogCabin {
namespace RPC {

class ZeromqClientRPC;
//forward declaration

/*
 ZeromqClientSession maintains a session to the remote server. It contains a queue of messages
 to support asynchronous id.
 ZeromqClientSession doesn't have a timer like ClientSession to null the request
 */
class ZeromqClientSession {
private:
	/*
	 This constructor is private because the class must be allocated in a
	 particular way. See #makeSession().
	 */
	ZeromqClientSession(const Address& address_, std::string smID);
public:
	/*
	 Return a new ZeromqClientSession object. This object is managed by a
	 std::shared_ptr to ensure that it remains alive while there are
	 outstanding RPCs.

	 param 
	 address: The RPC server address on which to connect.
	 smID: state machine ID
	 */
	static std::shared_ptr<ZeromqClientSession>
	makeSession(const Address& address, std::string smID);
	void sendRequest(ZeromqClientRPC *rpc, Buffer request);
	std::string getErrorMessage() const;
	std::string toString() const;

private:

	//64 bit is sufficient for now
	uint64_t nextMessageId;

	struct Response {
		/**
		 * Constructor.
		 */
		Response();
		/**
		 * Current state of the RPC.
		 */
		enum {
			/**
			 * Waiting for a reply from the server.
			 */
			WAITING,
			/**
			 * Received a reply (find it in #reply).
			 */
			HAS_REPLY,
			/**
			 * The RPC has been canceled by another thread.
			 */
			CANCELED,
		} status;
		/**
		 * The contents of the response. This is valid when
		 * #status is HAS_REPLY.
		 */
		Buffer reply;

	};

	std::unordered_map<uint64_t, Response*> responses;

	/**
	 * The RPC server address provided to the constructor.
	 */
	Address address;

	/*
	 This mutex protects all of the members of this class defined below this point.
	 */
	mutable std::mutex mutex;

	std::string errorMessage;

	void wait(const ZeromqClientRPC& rpc, int maxTries);
	void cancel(ZeromqClientRPC& rpc);
	void update(ZeromqClientRPC& rpc);

	//unique pointer to sender because only one session exists
	std::unique_ptr<ZeromqSender> sender;

	friend class ZeromqClientRPC;

};

}  // namespace LogCabin::RPC
}  // namespace LogCabin

#endif
