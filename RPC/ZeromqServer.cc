#include "ZeromqServer.h"
#include "RPC/ZeromqServerRPC.h"

namespace LogCabin {
namespace RPC {

ZeromqServer::ZeromqServer(std::shared_ptr<zmq::context_t> context)
		: services(), receiver(), context(context) {
	receiver = std::unique_ptr<RPC::ZeromqReceiver>(new RPC::ZeromqReceiver(context));
}
ZeromqServer::~ZeromqServer() {
	receiver.reset();
}

void ZeromqServer::close() {
	receiver->close();
}

std::string ZeromqServer::connect(std::string smID) {
	return receiver->connect(smID);
}
void ZeromqServer::registerService(uint16_t serviceId, std::shared_ptr<Service> service,
									uint32_t maxThreads, std::string smID) {
	services[serviceId] = std::make_shared<ThreadDispatchService>(service, 0, maxThreads, context, smID);
}

ZeromqOpaqueServerRPC ZeromqServer::waitForRPC(bool &breakRun) {
	int size = 0;
	uint64_t messageId;
	std::basic_string<unsigned char> client_id;
	std::string smID;
	void *data = receiver->wait(smID, size, messageId, client_id, breakRun);

	if (data == NULL)
		return ZeromqOpaqueServerRPC();
	Buffer buffer;
	buffer.setData(data, size, Buffer::deleteArrayFn<char>);
	ZeromqOpaqueServerRPC rpc(smID, messageId, client_id, std::move(buffer));
	return std::move(rpc);
}

void ZeromqServer::handleRPC(ZeromqOpaqueServerRPC opaqueRPC) {
	ZeromqServerRPC rpc(std::move(opaqueRPC), context);
	std::shared_ptr<Service> service;

	auto it = services.find(rpc.getService());
	if (it != services.end())
		service = it->second;

	if (service) {
		service->handleRPC(std::move(rpc));
	} else
		rpc.rejectInvalidRequest();
}

}  // namespace LogCabin::RPC
}  // namespace LogCabin
