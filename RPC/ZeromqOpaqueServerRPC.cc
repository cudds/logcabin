#include "ZeromqOpaqueServerRPC.h"
#include "Core/Debug.h"

namespace LogCabin {
namespace RPC {

ZeromqOpaqueServerRPC::ZeromqOpaqueServerRPC(std::string smID_,
		uint64_t messageId_, std::basic_string<unsigned char> client_id_,
		Buffer request_)
		: request(std::move(request_)), response(), client_id(client_id_), messageId(
				messageId_), smID(smID_) {
}
ZeromqOpaqueServerRPC::ZeromqOpaqueServerRPC()
		: request(), response(), client_id(), messageId(-1), smID() {
}

ZeromqOpaqueServerRPC::ZeromqOpaqueServerRPC(ZeromqOpaqueServerRPC&& other)
		: request(std::move(other.request)), response(
				std::move(other.response)), client_id(
				std::move(other.client_id)), messageId(
				std::move(other.messageId)), smID(std::move(other.smID)) {
}

ZeromqOpaqueServerRPC::~ZeromqOpaqueServerRPC() {
}

ZeromqOpaqueServerRPC&
ZeromqOpaqueServerRPC::operator=(ZeromqOpaqueServerRPC&& other) {
	request = std::move(other.request);
	response = std::move(other.response);
	client_id = std::move(other.client_id);
	messageId = std::move(other.messageId);
	smID = std::move(other.smID);
	return *this;
}

void ZeromqOpaqueServerRPC::sendReply(ZeromqPub& pub) {
	pub.sendMessage(client_id, messageId, response.getData(),
			response.getLength());
}

void ZeromqOpaqueServerRPC::closeSession() {
}

}  //end of RPC
}  //end of LogCabin
