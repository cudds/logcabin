#!/usr/bin/env python
'''
	The building/running environment is assumed to be ready and the nodes are accessible.
		It means the following software are installed: 
		OS: Ubuntu16.04.2(build), Ubuntu14.04.2(runtime)
		Software: python paramiko(build), g++-4.6(build), git(build), dh-autoreconf(build), build-essential(build, runtime), curl(build), scons 2.1.0(build)
		Library: zeromq 4.0.5(build), protobuf commitf473bb9(build), libcrypto++(build)
	This script has two subcommands, run and install
	run subcommand: this subcommand interact with the running cluster(nigo9731 is the default user created at running nodes) to prepare, run, kill logcabin daemon services
	install subcommand: this subcommand is used for other projects that depend on logcabin to initialize the building environment(basically install the header and library files)

	note: 
		1. libraries are necessary on the runtime, but this script elegantly provide a function to install them 
		2. default working directory at server is configured at RemoteDir

	Prepare tasks
		1. ssh public key should be stored on the server for no authentication
		2. create directory RemoteDir and set up the permission
		3. (optional) setup the directory RemoteDir for SSD
		4. (first time) enable library install by calling scpLibrary
'''
import argparse
import os
import paramiko
import shutil

#Constants
RemoteUser = "nigo9731"
LogCabin = "LogCabin"
LogSuffix = ".out"
ConfSuffix = ".conf"
RemoteDir = "~/Test/"
CurDir = "./"
RemoteLogCabin = RemoteDir + LogCabin
RemoteLogCabinConf = RemoteDir + LogCabin + ConfSuffix;
RemoteLogCabinOutput = RemoteDir + LogCabin + LogSuffix
LocalLogCabin = "./build/" + LogCabin
LocalLogCabinConf = CurDir + LogCabin + ConfSuffix;
LocalLogCabinOutputPrefix = CurDir + LogCabin + LogSuffix
initializeEnvironmentCommand = "export LD_LIBRARY_PATH=/usr/local/lib/:$LD_LIBRARY_PATH"

LibPath = "/usr/local/lib/"

def scp(direction, localFile, user, server, path):
	if direction == True:
		os.system("scp " + localFile + " " + user + "@" + server + ":" + path)
	else:
		os.system("scp " + user + "@" + server + ":" + path + " " + localFile)

def scpLogcabin(server):
	scp(True, LocalLogCabin, RemoteUser, server, RemoteLogCabin)
	scp(True, LocalLogCabinConf, RemoteUser, server, RemoteLogCabinConf)

def scpLog(server):
	scp(False, LocalLogCabinOutputPrefix + "." + server, RemoteUser, server, RemoteLogCabinOutput)

def runSSHCommand(server, username, command):
	client = paramiko.client.SSHClient()
	client.load_system_host_keys()
	client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
	client.connect(server, username=username)
	stdin, stdout, stderr = client.exec_command(initializeEnvironmentCommand+";"+command)
	stdoutAsString = []
	stderrAsString = []
	for line in stdout:
		stdoutAsString.append(line)
	for line in stderr:
		stderrAsString.append(line)
	return stdoutAsString, stderrAsString

def runSSHCommandAsBackground(server, username, command, outputFile):
	return runSSHCommand(server, username, command+" &>"+outputFile+" &")

def outputPrint(stdout, stderr):
	print "stdout:\n"
	for line in stdout:
		print "\t" + line
	print "stderr:\n"
	for line in stderr:
		print "\t" + line

parser = argparse.ArgumentParser()
subparsers = parser.add_subparsers()
runtime_parser = subparsers.add_parser('run', help='deployment for runtime environment')
runtime_parser.set_defaults(subcommand='runtime')
runtime_parser.add_argument("-m","--mode",dest="mode",help="prepare: initialize the storage; start: start the service; log: collecting logs; kill: kill the service", choices = ["prepare", "start", "log", "kill"], required =True)
runtime_parser.add_argument("-c","--cluster",dest="cluster",help="cluster config file: ip list", nargs =1)
build_parser = subparsers.add_parser('install', help='install logcabin locally')
build_parser.set_defaults(subcommand='install')

args = parser.parse_args()

if args.subcommand == 'runtime':
	ips = []
	f = open(args.cluster[0],'r')
	for line in f:
		ips.append(line.strip())
	print ips

	if args.mode == "prepare":
		for ip in ips:
			scpLogcabin(ip)
			runSSHCommand(ip, RemoteUser, "mkdir /users/nigo9731/Test/Logcabinlog; rm -rf /users/nigo9731/Test/Logcabinlog/*")
	elif args.mode == "start":
		for ip in ips:
			remoteCommand = "taskset 0x1 " + RemoteLogCabin + " -c " + RemoteLogCabinConf
			runSSHCommandAsBackground(ip, RemoteUser, remoteCommand, RemoteLogCabinOutput)
	elif args.mode == "log":
		for ip in ips:
			scpLog(ip)
	elif args.mode == "kill":
		for ip in ips:
			runSSHCommand(ip, RemoteUser, "sudo kill -9 $(ps -ef|grep LogCabin|head -n 1|awk '{print $2}')")
			runSSHCommand(ip, RemoteUser, "sudo kill -9 $(ps -ef|grep LogCabin|tail -n 1|awk '{print $2}')")
else:
	try:
		os.mkdir("/usr/local/include/logcabin")
		os.mkdir("/usr/local/include/logcabin/Client")
		os.mkdir("/usr/local/include/logcabin/RPC")
		os.mkdir("/usr/local/include/logcabin/RPC/zeromq")
	except OSError:
		print "Overwrite existing include files"
	shutil.copyfile("Client/Client.h","/usr/local/include/logcabin/Client/Client.h")
	shutil.copyfile("Client/ClientDataStructure.h","/usr/local/include/logcabin/Client/ClientDataStructure.h")
	shutil.copyfile("Client/DataStore.h","/usr/local/include/logcabin/Client/DataStore.h")
	shutil.copyfile("Client/KDTree.h","/usr/local/include/logcabin/Client/KDTree.h")
	shutil.copyfile("RPC/DaemonSendHelper.h","/usr/local/include/logcabin/RPC/DaemonSendHelper.h")
	shutil.copyfile("RPC/zeromq/ZeromqDaemonSend.h","/usr/local/include/logcabin/RPC/zeromq/ZeromqDaemonSend.h")
	shutil.copyfile("build/liblogcabin.a","/usr/local/lib/liblogcabin.a")
	shutil.copyfile("build/liblogcabin.so","/usr/local/lib/liblogcabin.so")
