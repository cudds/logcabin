Overview
========

LogCabin is a distributed system that provides a small amount of highly
replicated, consistent storage. It is a reliable place for other distributed
systems to store their core metadata and is helpful in solving cluster
management issues. Although its key functionality is in place, LogCabin is not
yet recommended for actual use. LogCabin is released under the permissive ISC
license (which is equivalent to the Simplified BSD License).

For more info, visit the project page at
https://ramcloud.stanford.edu/wiki/display/logcabin/LogCabin

LogCabin uses the Raft consensus algorithm internally, which is described in
https://ramcloud.stanford.edu/raft.pdf

This README will walk you through how to compile and run LogCabin.

Building
========

Pre-requisites:

- git (v1.7 is known to work)
- scons (v2.1 is known to work)
- g++ (v4.4 and up should work)
- protobuf (v3.0.0 is known to work)
- crypto++ (v5.6.1 is known to work)
- doxygen (optional; v1.8.7 is known to work)

Get the source code::

 git clone git@git.cs.colorado.edu:nigo9731/logcabin.git
 cd logcabin
 git submodule update --init


Build the client library, server binary, and unit tests::

 scons

For custom build environments, you can place your configuration variables in
``Local.sc``. For example, that file might look like::

 BUILDTYPE='DEBUG'
 CXXFLAGS=['-Wno-error']

To see which configuration parameters are available, run::

 scons --help

Running basic tests
===================

It's a good idea to run the included unit tests before proceeding::

 build/test/test

You can also run some system-wide tests. This first command runs the smoke
tests against an in-memory database that is embedded into the LogCabin client
(no servers are involved)::

 build/Examples/SmokeTest --mock && echo 'Smoke test completed successfully'

To run the same smoke test against a real LogCabin cluster, you'll need to go
through some more configuration. This can be found after the Running section.

Running a real cluster
======================

This section shows you how to run the HelloWorld example program against a
three-server LogCabin cluster. 

First, launch three instances and write their ip addresses in a file(assume at ./ips) formatted as follows::
 192.168.2.1
 192.168.2.2
 192.168.2.3

Second, prepare the cluster like moving LogCabin executable file to the cluster
 ./Deploy.py run -m prepare -c ips
For fast log, in each instance mount in memory disk
 mount -t tmpfs -o size=512m tmpfs /mnt/ramdisk

Now start the daemons
 ./Deploy.py run -m start -c ips

Now create your first replicated state machine
 ./build/Examples/Spectrum -k 1 -s sm1 -p 61022

Now use the reconfiguration command to add the second and third servers to the
cluster::

  build/Examples/Reconfigure -c 192.168.2.1:61023 192.168.2.1:61023 192.168.2.2:61023 192.168.2.3:61023

This Reconfigure command is a special LogCabin client. It looks up the cluster
using DNS and asks the leader to reconfigure the cluster to the addresses given
on its command line. If this succeeded, you should see that the first server
has added the others to the cluster, and the second and third servers are now
participating. It should have output something like::

 Configuration 1:
 - 1: 192.168.2.1:61023
 
 Reconfiguration OK
 Configuration 4:
 - 1: 192.168.2.1:61023
 - 2: 192.168.2.2:61023
 - 3: 192.168.2.3:61023

Finally, you can run a LogCabin client to exercise the cluster::

 build/Examples/HelloWorld

That program doesn't do anything very interesting. You should be able to kill
one server at a time and maintain availability, or kill more and restart
them and maintain safety (with an availability hiccup).

If you have your own application, you can link it against
``build/liblogcabin.a``. You'll also need to link against the following
libraries:

- pthread
- protobuf
- cryptopp

Documentation
=============

To build the documentation from the source code, run::

 scons docs

The resulting HTML files will be placed in ``docs/doxygen``.

The Raft consensus algorithm is described in
https://ramcloud.stanford.edu/raft.pdf

Release
============

RPC are replaced with Zeromq
Multiple state machines within a single node
