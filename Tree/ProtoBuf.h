/* Copyright (c) 2012 Stanford University
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR(S) DISCLAIM ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL AUTHORS BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include "build/Protocol/Client.pb.h"
#include "Tree/Tree.h"
#include "Tree/DataStore.h" 
#include "Tree/KDTreeStore.h"

#ifndef LOGCABIN_TREE_PROTOBUF_H
#define LOGCABIN_TREE_PROTOBUF_H

namespace LogCabin {
namespace Tree {
namespace ProtoBuf {

/**
 * Respond to a read-only request to query a Tree.
 */
void
readOnlyTreeRPC(const Tree& tree,
		const Protocol::Client::ReadOnlyTree::Request& request,
		Protocol::Client::ReadOnlyTree::Response& response);

//added by ning
void readOnlyDataRPC(const DataStore& data,
		const Protocol::Client::ReadOnlyData::Request& request,
		Protocol::Client::ReadOnlyData::Response& response);
void readOnlyKDRPC(const KDTreeStore& tree,
		const Protocol::Client::ReadOnlyKD::Request& request,
		Protocol::Client::ReadOnlyKD::Response& response);

/**
 * Respond to a read-write operation on a Tree.
 */
void
readWriteTreeRPC(Tree& tree,
		const Protocol::Client::ReadWriteTree::Request& request,
		Protocol::Client::ReadWriteTree::Response& response);

//added by ning
void readWriteDataRPC(DataStore& data,
		const Protocol::Client::ReadWriteData::Request& request,
		Protocol::Client::ReadWriteData::Response& response);
void readWriteKDRPC(KDTreeStore& tree,
		const Protocol::Client::ReadWriteKD::Request& request,
		Protocol::Client::ReadWriteKD::Response& response);

}  // namespace LogCabin::Tree::ProtoBuf
}  // namespace LogCabin::Tree
}  // namespace LogCabin

#endif // LOGCABIN_TREE_PROTOBUF_H
