//written by ning gao in University of Colorado Boulder 
//<nigo9731@colorado.edu>
#include "build/Tree/Snapshot.pb.h"
#include "KDTreeStore.h"
#include <cassert>
#include "Core/StringUtil.h"
#include "Core/Debug.h"

#include <iostream>

namespace LogCabin {
namespace Tree {
using Core::StringUtil::format;

KDTreeStore::KDTreeStore()
		: kdtree(std::ptr_fun(tac)) {

}

Result KDTreeStore::insert(double d1, double d2) {
	Result result;
	kdtree.insert(twin(d1, d2));
	result.status = Status::OK;
	return result;
}

Result KDTreeStore::erase(double d1, double d2) {
	Result result;
	kdtree.erase(twin(d1, d2));
	result.status = Status::OK;
	return result;
}

Result KDTreeStore::find(double d1, double d2, double range,
		std::vector<twin>& points) const {
	Result result;
	kdtree.find_within_range(twin(d1, d2), range, std::back_inserter(points));
	result.status = Status::OK;
	return result;
}

void KDTreeStore::dumpSnapshot(
		google::protobuf::io::CodedOutputStream& stream) const {
	KDTree::_Node<twin> const* root = kdtree._M_get_root();
	if (root == NULL) {
		stream.WriteLittleEndian32(-1);
		return;
	}
	Snapshot::Node node;
	node.mutable_value()->set_dimension1((root->_M_value)[0]);
	node.mutable_value()->set_dimension2((root->_M_value)[1]);
	node.set_left((root->_M_left != NULL));
	node.set_right((root->_M_right != NULL));
	stream.WriteLittleEndian32(node.ByteSize());
	node.SerializeWithCachedSizes(&stream);
	if (root->_M_left != NULL)
		_dumpSnapshot(static_cast<KDTree::_Node<twin>*>(root->_M_left), stream);
	if (root->_M_right != NULL)
		_dumpSnapshot(static_cast<KDTree::_Node<twin>*>(root->_M_right),
				stream);
}

void KDTreeStore::_dumpSnapshot(KDTree::_Node<twin>* root,
		google::protobuf::io::CodedOutputStream& stream) const {
	Snapshot::Node node;
	node.mutable_value()->set_dimension1((root->_M_value)[0]);
	node.mutable_value()->set_dimension2((root->_M_value)[1]);
	node.set_left((root->_M_left != NULL));
	node.set_right((root->_M_right != NULL));
	node.ByteSize();
	node.SerializeWithCachedSizes(&stream);
	if (root->_M_left != NULL)
		_dumpSnapshot(static_cast<KDTree::_Node<twin>*>(root->_M_left), stream);
	if (root->_M_right != NULL)
		_dumpSnapshot(static_cast<KDTree::_Node<twin>*>(root->_M_right),
				stream);
}

void KDTreeStore::loadSnapshot(google::protobuf::io::CodedInputStream& stream) {
	kdtree.clear();
	bool ok = stream.ReadLittleEndian32(&numBytes);
	if (!ok)
		PANIC("couldn't read snapshot");
	if (numBytes == -1)
		return;
	Snapshot::Node node;
	auto limit = stream.PushLimit(numBytes);
	ok = node.MergePartialFromCodedStream(&stream);
	stream.PopLimit(limit);
	if (!ok)
		PANIC("couldn't read snapshot");
	kdtree.insert(twin(node.value().dimension1(), node.value().dimension2()));
	KDTree::_Node<twin>* root = kdtree._M_get_root();
	if (node.left() == true)
		_loadSnapshot(true, root, stream);
	if (node.right() == true)
		_loadSnapshot(false, root, stream);
	kdtree._M_reset_leftmost();
	kdtree._M_reset_rightmost();
}

void KDTreeStore::_loadSnapshot(bool isLeft, KDTree::_Node<twin>* parent,
		google::protobuf::io::CodedInputStream& stream) {
	Snapshot::Node node;
	auto limit = stream.PushLimit(numBytes);
	bool ok = node.MergePartialFromCodedStream(&stream);
	stream.PopLimit(limit);
	if (!ok)
		PANIC("couldn't read snapshot");
	KDTree::_Node<twin>* cur;
	if (isLeft == true) {
		kdtree._M_insert_left(parent,
				twin(node.value().dimension1(), node.value().dimension2()));
		cur = static_cast<KDTree::_Node<twin>*>(parent->_M_left);
	} else {
		kdtree._M_insert_right(parent,
				twin(node.value().dimension1(), node.value().dimension2()));
		cur = static_cast<KDTree::_Node<twin>*>(parent->_M_right);
	}
	if (node.left() == true)
		_loadSnapshot(true, cur, stream);
	if (node.right() == true)
		_loadSnapshot(false, cur, stream);
}

}
}
