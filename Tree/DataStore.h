//written by ning gao in University of Colorado Boulder 
//<nigo9731@colorado.edu>
#include "DataStructure.h"
#include <string>
#include <map>
#include <google/protobuf/io/coded_stream.h>

#ifndef LOGCABIN_TREE_DATASTORE_H
#define LOGCABIN_TREE_DATASTORE_H
namespace LogCabin {
namespace Tree {
/*
 DataStore provides a key value map store in logcabin
 */
class DataStore {
public:
	//Constructor
	DataStore();
	void
	loadSnapshot(google::protobuf::io::CodedInputStream& stream);
	void
	dumpSnapshot(google::protobuf::io::CodedOutputStream& stream) const;
	Result
	read(const std::string& key, std::string& value) const;
	Result
	write(const std::string& key, const std::string& value);
private:
	std::map<std::string, std::string> keyvalue;

};
}
}

#endif
