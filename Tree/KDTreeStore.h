//written by ning gao in University of Colorado Boulder 
//<nigo9731@colorado.edu>
#include "DataStructure.h"
#include <string>
#include <vector>
#include <google/protobuf/io/coded_stream.h>
#include "KD-Tree/kdtree.hpp"

#ifndef LOGCABIN_TREE_KDTree_H
#define LOGCABIN_TREE_KDTree_H
namespace LogCabin {
namespace Tree {

typedef KDTree::KDTree<2, twin,
		std::pointer_to_binary_function<twin, size_t, double> > tree_type;

inline double tac(twin t, size_t k) {
	return t[k];
}

/*
 KDTreeStore provides a kd-tree data structure in logcabin
 */
class KDTreeStore {
public:
	//Constructor
	KDTreeStore();
	void
	loadSnapshot(google::protobuf::io::CodedInputStream& stream);
	void
	_loadSnapshot(bool isLeft, KDTree::_Node<twin>* parent,
			google::protobuf::io::CodedInputStream& stream);
	void
	dumpSnapshot(google::protobuf::io::CodedOutputStream& stream) const;
	void
	_dumpSnapshot(KDTree::_Node<twin> *root,
			google::protobuf::io::CodedOutputStream& stream) const;
	Result
	insert(double d1, double d2);
	Result
	erase(double d1, double d2);
	Result
	find(double d1, double d2, double range, std::vector<twin>& points) const;
private:
	tree_type kdtree;
	uint32_t numBytes;

};
}
}

#endif
