//written by ning gao in University of Colorado Boulder 
//<nigo9731@colorado.edu>
#include "build/Tree/Snapshot.pb.h"
#include "DataStore.h"
#include <cassert>
#include "Core/StringUtil.h"
#include "Core/Debug.h"

namespace LogCabin {
namespace Tree {
using Core::StringUtil::format;

DataStore::DataStore()
		: keyvalue() {

}

Result DataStore::read(const std::string& key, std::string& value) const {
	Result result;
	value.clear();
	auto it = keyvalue.find(key);
	//key not found
	if (it == keyvalue.end()) {
		result.status = Status::KEY_NOT_FOUND;
		result.error = format("%s key is not found", key.c_str());
		return result;
	}
	value = it->second;
	result.status = Status::OK;
	return result;
}

Result DataStore::write(const std::string& key, const std::string& value) {
	Result result;
	//std::cout<<"in DataStore key:value is "<<key<<" "<<value<<std::endl;
	keyvalue.insert(std::make_pair(key, value));
	result.status = Status::OK;
	return result;
}

void DataStore::dumpSnapshot(
		google::protobuf::io::CodedOutputStream& stream) const {
	Snapshot::KeyValueContent keyvaluecontent;
	for (auto it = keyvalue.begin(); it != keyvalue.end(); ++it) {
		keyvaluecontent.add_keysvalues(it->first);
		keyvaluecontent.add_keysvalues(it->second);
	}
	int size = keyvaluecontent.ByteSize();
	stream.WriteLittleEndian32(size);
	keyvaluecontent.SerializeWithCachedSizes(&stream);
}

void DataStore::loadSnapshot(google::protobuf::io::CodedInputStream& stream) {
	bool ok = true;
	uint32_t numBytes = 0;
	ok = stream.ReadLittleEndian32(&numBytes);
	if (!ok)
		PANIC("couldn't read snapshot");
	Snapshot::KeyValueContent keyvaluecontent;
	auto limit = stream.PushLimit(numBytes);
	ok = keyvaluecontent.MergePartialFromCodedStream(&stream);
	stream.PopLimit(limit);
	if (!ok)
		PANIC("couldn't read snapshot");
	bool even = false;
	std::string key;
	for (auto it = keyvaluecontent.keysvalues().begin();
			it != keyvaluecontent.keysvalues().end(); ++it) {
		if (!even) {
			key = *it;
			even = true;
		} else {
			even = false;
			keyvalue[key] = *it;
		}
	}
}

}
}
