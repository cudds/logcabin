//written by ning gao in University of Colorado Boulder 
//<nigo9731@colorado.edu>
#include <cmath>
#ifndef LOGCABIN_CLIENT_DATASTRUCTURE_H
#define LOGCABIN_CLIENT_DATASTRUCTURE_H
namespace LogCabin {
namespace Client {

/**
 * Status codes returned by Tree operations.
 */
enum class Status {

	/**
	 * The operation completed successfully.
	 */
	OK = 0,

	/**
	 * If an argument is malformed (for example, a path that does not start
	 * with a slash).
	 */
	INVALID_ARGUMENT = 1,

	/**
	 * If a file or directory that is required for the operation does not
	 * exist.
	 */
	LOOKUP_ERROR = 2,

	/**
	 * If a directory exists where a file is required or a file exists where
	 * a directory is required.
	 */
	TYPE_ERROR = 3,

	/**
	 * A predicate which was previously set on operations with
	 * Tree::setCondition() was not satisfied.
	 */
	CONDITION_NOT_MET = 4,
};

/**
 * Print a status code to a stream.
 */
std::ostream&
operator<<(std::ostream& os, Status status);

/**
 * Returned by Tree operations; contain a status code and an error message.
 */
struct Result {
	/**
	 * Default constructor. Sets status to OK and error to the empty string.
	 */
	Result()
			: status(Status::OK), error() {
	}
	/**
	 * A code for whether an operation succeeded or why it did not. This is
	 * meant to be used programmatically.
	 */
	Status status;
	/**
	 * If status is not OK, this is a human-readable message describing what
	 * went wrong.
	 */
	std::string error;
};

/**
 * Base class for LogCabin client exceptions.
 */
class Exception: public std::runtime_error {
public:
	explicit Exception(const std::string& error);
};

/**
 * See Status::INVALID_ARGUMENT.
 */
class InvalidArgumentException: public Exception {
public:
	explicit InvalidArgumentException(const std::string& error);
};

/**
 * See Status::LOOKUP_ERROR.
 */
class LookupException: public Exception {
public:
	explicit LookupException(const std::string& error);
};

/**
 * See Status::TYPE_ERROR.
 */
class TypeException: public Exception {
public:
	explicit TypeException(const std::string& error);
};

/**
 * See Status::CONDITION_NOT_MET.
 */
class ConditionNotMetException: public Exception {
public:
	explicit ConditionNotMetException(const std::string& error);
};

struct twin {
	typedef double value_type;

	twin(value_type a, value_type b) {
		d[0] = a;
		d[1] = b;
	}

	twin(const twin & x) {
		d[0] = x.d[0];
		d[1] = x.d[1];
	}

	~twin() {
	}

	double distance_to(twin const& x) const {
		double dist = 0;
		for (int i = 0; i != 2; ++i)
			dist += (d[i] - x.d[i]) * (d[i] - x.d[i]);
		return std::sqrt(dist);
	}

	inline value_type operator[](size_t const N) const {
		return d[N];
	}

	value_type d[2];
};

}
}

#endif
