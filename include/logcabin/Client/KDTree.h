//written by Ning
#include <mutex>
#include <memory>
#include <string>
#include <vector>
#include "logcabin/Client/ClientDataStructure.h"

#ifndef LOGCABIN_CLIENT_KDTREE_H
#define LOGCABIN_CLIENT_KDTREE_H
namespace LogCabin {
namespace Client {

class ClientImplBase;
// forward declaration

class KDTree {
private:
	KDTree(std::shared_ptr<ClientImplBase> clientImpl);
public:
	/// Copy constructor.
	KDTree(const KDTree& other);
	/// Assignment operator.
	KDTree& operator=(const KDTree& other);

	//insert, erase, find_within_range
	Result insert(const twin& data);
	Result erase(const twin& data);
	Result find_within_range(const twin& data, double range,
			std::vector<twin>& result);

private:
	std::shared_ptr<ClientImplBase> getClientImp() const;
	mutable std::mutex mutex;
	std::shared_ptr<ClientImplBase> clientImpl;
	friend class Cluster;
};

}
}
#endif
