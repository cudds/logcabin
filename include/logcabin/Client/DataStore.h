#include <mutex>
#include <memory>
#include <string>
#include "logcabin/Client/ClientDataStructure.h"

#ifndef LOGCABIN_CLIENT_DATASTORE_H
#define LOGCABIN_CLIENT_DATASTORE_H
namespace LogCabin {
namespace Client {

class ClientImplBase;
// forward declaration

class DataStore {
private:
	DataStore(std::shared_ptr<ClientImplBase> clientImpl);
public:
	/// Copy constructor.
	DataStore(const DataStore& other);
	/// Assignment operator.
	DataStore& operator=(const DataStore& other);

	Result read(const std::string& key, std::string& value) const;
	Result write(const std::string& key, const std::string& value);

private:
	std::shared_ptr<ClientImplBase> clientImpl;
	friend class Cluster;
};

}
}
#endif
