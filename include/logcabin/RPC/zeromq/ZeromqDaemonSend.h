//written by ning gao in University of Colorado Boulder 
//<nigo9731@colorado.edu>

#include "zmsg.hpp"
#include <string>
#include <memory>

#ifndef LOGCABIN_RPC_ZEROMQDAEMONSEND_H
#define LOGCABIN_RPC_ZEROMQDAEMONSEND_H

namespace LogCabin {
namespace RPC {
/*
 ZeromqDaemonSend is a wrapper class around zeromq to open a socket to connect to a remote Daemon process
 */

class ZeromqDaemonSend {
public:
	/*
	 Constructor 
	 Context is shared so that inproc works
	 */
	ZeromqDaemonSend(std::shared_ptr<zmq::context_t> context);
	~ZeromqDaemonSend();
	std::string connect(std::string address, std::string port);
	/*
	 sendMessage sends a response back for the previous request.
	 */
	void sendMessage(unsigned char *data, uint32_t length);
private:
	zmq::socket_t socket;
};

}  //end of RPC
}  //end of LogCabin

#endif
