//written by ning gao in University of Colorado Boulder 
//<nigo9731@colorado.edu>
#include "logcabin/RPC/zeromq/ZeromqDaemonSend.h"

#ifndef LOGCABIN_SERVER_DAEMONSENDHELPER_H
#define LOGCABIN_SERVER_DAEMONSENDHELPER_H
namespace LogCabin {
namespace RPC {

/*
 DaemonSendHelper is used by Daemon thread and client program to talk to another Daemon thread for two kinds of requests: create and delete
 */

class DaemonSendHelper {
public:
	enum Kind {
		CREATE, DELETE
	};
	DaemonSendHelper(std::shared_ptr<zmq::context_t> context);
	~DaemonSendHelper();
	bool send(Kind kind, std::string servers, std::string smID,
			uint16_t serverID, std::string port);

private:
	std::shared_ptr<zmq::context_t> context;

};

}  //end of Server
}  //end of LogCabin
#endif
